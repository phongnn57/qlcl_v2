package com.company.meboo.meboo.presenter.mChonKhuVucPresenter2;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.KhuVuc;

import java.util.ArrayList;

public interface ChonKhuVucPresenter2 extends BasePresenter{
    ArrayList<KhuVuc> getListKhuVuc();

    void chonKhuVuc(int position);
}
