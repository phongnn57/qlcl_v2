package com.company.meboo.meboo.ui.activity.mYeuCauGiaHan;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.model.model.object.DataReportDetail;
import com.company.meboo.meboo.presenter.mYeuCauGiaHanPresenter.YeuCauGiaHanPresenter;

public interface YeuCauGiaHanView extends BaseView<YeuCauGiaHanPresenter>{
    Context gContext();

    void setData(DataReportDetail dataReportDetail);
}
