package com.company.meboo.meboo.ui.activity.mThemKhuVucActivity;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mThemKhuVucPresenter.ThemKhuVucPresenter;

public interface ThemKhuVucView extends BaseView<ThemKhuVucPresenter>{
    void notifyRegionData();
    void finishActivity();

    Context gContext();
}
