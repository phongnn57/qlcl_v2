package com.company.meboo.meboo.firebase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.company.meboo.meboo.ui.activity.mLoginActivity.LoginActivity;
import com.company.meboo.meboo.ui.activity.mThongBaoChiTiet.ThongBaoChiTietActivity;
import com.company.meboo.meboo.util.Key;

public class NotificationClickReceiver extends BroadcastReceiver {
    private static final String TAG = "NotificationClick";

    @Override
    public void onReceive(Context context, Intent intent) {

        Bundle extras = intent.getExtras();
        Log.d(TAG, "onReceive: " + extras);
        Log.d(TAG, "onReceive:id: " + extras.get(Key.ID));


        String type = (String) extras.get(Key.TYPE);

        Log.d(TAG, "onReceive:type: " + type);

        switch (type){
            case "1":
                Log.d("type","type: "+type);
                Intent i = new Intent(context.getApplicationContext(), ThongBaoChiTietActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra(Key.ID, Integer.parseInt(extras.get(Key.ID) + ""));
                context.getApplicationContext().startActivity(i);
                break;


            default:
                context.getApplicationContext().startActivity(new Intent(context.getApplicationContext(), LoginActivity.class));
                break;
        }



    }
}
