package com.company.meboo.meboo.model.model.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuarterReport {

    @SerializedName("quarter_id")
    @Expose
    private Integer quarterId;
    @SerializedName("quarter_name")
    @Expose
    private String quarterName;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("good")
    @Expose
    private Integer good;
    @SerializedName("bad")
    @Expose
    private Integer bad;

    public Integer getQuarterId() {
        return quarterId;
    }

    public void setQuarterId(Integer quarterId) {
        this.quarterId = quarterId;
    }

    public String getQuarterName() {
        return quarterName;
    }

    public void setQuarterName(String quarterName) {
        this.quarterName = quarterName;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getGood() {
        return good;
    }

    public void setGood(Integer good) {
        this.good = good;
    }

    public Integer getBad() {
        return bad;
    }

    public void setBad(Integer bad) {
        this.bad = bad;
    }

}