package com.company.meboo.meboo.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DownloadManager;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;

public class Helper {
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void showMessage(String string, Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Thông báo");
        builder.setMessage(string);
        builder.setCancelable(false);
        builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    public static void showDataPickerDialog(Context context, final OnDateSetListener onDateSetListener){
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                calendar.set(year,month,dayOfMonth);
                onDateSetListener.onDateSet(calendar);
            }
        },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE));
        datePickerDialog.show();
    }

    public static void showTimePickerDialog(Context context, final OnTimeSetListener onTimeSetListener){
        final Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                onTimeSetListener.onTimeSet(calendar);
            }
        },hour,minute,true);
        timePickerDialog.show();
    }


    public interface  OnDateSetListener{
        void onDateSet(Calendar c);
    }

    public interface OnTimeSetListener{
        void onTimeSet(Calendar c);
    }

    public static String getTime(Date deadline, Date extendTime) {
        StringBuilder stringBuilder = new StringBuilder();

        //milliseconds
        long different = extendTime.getTime() - deadline.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        Log.d("deadline",elapsedDays+":"+elapsedHours+":"+elapsedMinutes+":"+elapsedSeconds) ;

        if(elapsedDays>0){
            stringBuilder.append(elapsedDays + " ngày ");
        }
        if(elapsedHours>0){
            stringBuilder.append(elapsedHours + " giờ ");
        }
        if(elapsedMinutes>0){
            stringBuilder.append(elapsedMinutes + " phút ");
        }
        if(elapsedSeconds>0){
            stringBuilder.append(elapsedSeconds + " giây");
        }

        return stringBuilder.toString();
    }

}
