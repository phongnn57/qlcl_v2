package com.company.meboo.meboo.ui.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.model.model.object.QuarterReport;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;

import java.util.ArrayList;
import java.util.List;

public class BaoCaoAdapter extends RecyclerView.Adapter<BaoCaoAdapter.ViewHolder>{
    Context context;
    ArrayList<QuarterReport> arrayReport;

    public BaoCaoAdapter(Context context, ArrayList<QuarterReport> arrayReport) {
        this.context = context;
        this.arrayReport = arrayReport;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_report,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if(arrayReport.get(position).getTotal()==0){
            holder.pieChart.setVisibility(View.GONE);
            holder.tvQuarterName.setVisibility(View.VISIBLE);
            holder.tvQuarterName.setText(arrayReport.get(position).getQuarterName());
        }else{
            holder.tvQuarterName.setVisibility(View.GONE);
            holder.pieChart.setVisibility(View.VISIBLE);
            setupPieChart(position, holder.pieChart);
        }

        holder.tvGood.setText(arrayReport.get(position).getGood()+"");
        holder.tvBad.setText(arrayReport.get(position).getBad()+"");
        holder.tvTotal.setText(arrayReport.get(position).getTotal()+"");
    }

    private void setupPieChart(int position, PieChart pieChart) {
        List<PieEntry> entries = new ArrayList<>();
        entries.add(new PieEntry(arrayReport.get(position).getGood(),"Đạt"));
        entries.add(new PieEntry(arrayReport.get(position).getBad(),"Không đạt"));

        PieDataSet dataSet = new PieDataSet(entries, "Label");
        dataSet.setColors(new int[] { R.color.dat,R.color.khongdat},context);

        PieData pieData = new PieData(dataSet);
        pieData.setValueFormatter(new PercentFormatter());
        pieData.setValueTextColor(Color.parseColor("#FFFFFF"));
        pieData.setValueTextSize(14f);
        pieChart.setData(pieData);

        pieChart.setUsePercentValues(true);
        pieChart.setCenterText(arrayReport.get(position).getQuarterName());
        pieChart.setCenterTextSize(18f);
        pieChart.setCenterTextTypeface(Typeface.DEFAULT_BOLD);
        pieChart.getLegend().setEnabled(false);
        pieChart.getDescription().setEnabled(false);
        pieChart.animateXY(1500, 1500);
    }

    @Override
    public int getItemCount() {
        return arrayReport.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvQuarterName, tvGood, tvBad, tvTotal;
        PieChart pieChart;
        public ViewHolder(View itemView) {
            super(itemView);
            tvQuarterName = itemView.findViewById(R.id.tvQuarterName);
            tvGood = itemView.findViewById(R.id.tvGood);
            tvBad = itemView.findViewById(R.id.tvBad);
            tvTotal = itemView.findViewById(R.id.tvTotal);
            pieChart = itemView.findViewById(R.id.pieChart);
        }
    }
}
