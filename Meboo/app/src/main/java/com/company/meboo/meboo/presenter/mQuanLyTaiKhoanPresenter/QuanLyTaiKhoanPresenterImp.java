package com.company.meboo.meboo.presenter.mQuanLyTaiKhoanPresenter;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.Account;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.network.Response;
import com.company.meboo.meboo.ui.activity.mQuanLyTaiKhoanActivity.QuanLyTaiKhoanView;
import com.company.meboo.meboo.util.PrefUtil;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class QuanLyTaiKhoanPresenterImp extends BasePresenterImpl<QuanLyTaiKhoanView> implements QuanLyTaiKhoanPresenter {
    ArrayList<Account> listAccount = new ArrayList<>();

    public QuanLyTaiKhoanPresenterImp(QuanLyTaiKhoanView view) {
        super(view);
        initListAccount();
    }

    @Override
    public ArrayList<Account> getAccountList() {
        return listAccount;
    }

    public void initListAccount(){
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().getAccountList(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<ArrayList<Account>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<ArrayList<Account>> arrayListResponse) {
                        listAccount.clear();
                        listAccount.addAll(arrayListResponse.getData());
                        getView().notifyData();
                    }
                });


    }
}
