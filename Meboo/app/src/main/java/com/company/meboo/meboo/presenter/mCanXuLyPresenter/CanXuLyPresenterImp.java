package com.company.meboo.meboo.presenter.mCanXuLyPresenter;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.ui.fragment.CanXuLyFragment.CanXuLyView;

public class CanXuLyPresenterImp extends BasePresenterImpl<CanXuLyView>  implements CanXuLyPresenter{
    public CanXuLyPresenterImp(CanXuLyView view) {
        super(view);
    }
}
