package com.company.meboo.meboo.ui.activity.mBaoCaoXuLy;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.model.model.object.CheckList;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.presenter.mBaoCaoXuLyPresenter.BaoCaoXuLyPresenter;
import com.company.meboo.meboo.presenter.mBaoCaoXuLyPresenter.BaoCaoXuLyPresenterImp;
import com.company.meboo.meboo.ui.adapter.ImageAdapter;
import com.company.meboo.meboo.ui.adapter.RegionAdapter;
import com.company.meboo.meboo.util.Helper;
import com.company.meboo.meboo.util.Key;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class BaoCaoXuLyActivity extends BaseActivity<BaoCaoXuLyPresenter> implements BaoCaoXuLyView, View.OnClickListener {
    CheckList currentCheckList;
    TextView tvWorkType, tvQuarter, tvDesc, tvDeadLine, tvRecommend;
    TextView tvAddImage;
    EditText edtDesc;
    TextView tvAddRegion;
    ArrayList<String> listImageUri = new ArrayList<>();
    ImageAdapter imageAdapter;
    RecyclerView rvImage;
    RecyclerView rvSubRegion;
    RegionAdapter regionAdapter;
    Button btnConfirm;
    ArrayList<SubRegion> listRegions = new ArrayList<>();
    ArrayList<SubRegion> listRegionsFromApi = new ArrayList<>();
    String tvTime = "";
    ArrayList<String> filePaths = new ArrayList<>();

    @Override
    public int getContentViewId() {
        return R.layout.activity_bao_cao_xu_ly;
    }

    @Override
    public void initializeComponents() {
        setupToolbar();
        getPresenter().callApiListRegion();
        listRegionsFromApi = getPresenter().getListRegion();

        tvDesc = findViewById(R.id.tvDesc);
        tvRecommend = findViewById(R.id.tvRecommend);
        edtDesc = findViewById(R.id.edtDesc);
        btnConfirm = findViewById(R.id.btnConfirm);
        tvDeadLine = findViewById(R.id.tvDeadLine);
        tvAddRegion = findViewById(R.id.tvAddRegion);
        tvWorkType = findViewById(R.id.tvWorkType);
        tvQuarter = findViewById(R.id.tvQuarter);
        tvAddImage = findViewById(R.id.tvAddImage);
        rvImage = findViewById(R.id.rvImage);
        rvSubRegion = findViewById(R.id.rvSubRegion);
        currentCheckList = (CheckList) getIntent().getSerializableExtra(Key.BAD_CLICK);
        tvWorkType.setText(currentCheckList.getWorkType());
        tvQuarter.setText(currentCheckList.getQuarter());
        tvDesc.setText(currentCheckList.getDesc());


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvImage.setLayoutManager(linearLayoutManager);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvSubRegion.setLayoutManager(linearLayoutManager2);

        tvAddImage.setOnClickListener(this);
        tvAddRegion.setOnClickListener(this);
        tvDeadLine.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);

        regionAdapter = new RegionAdapter(this, listRegions, R.layout.region_item);
        regionAdapter.setOnItemClickListener(new RegionAdapter.OnItemClickListener() {
            @Override
            public void onClickDelete(int position) {
                listRegions.remove(position);
                regionAdapter.notifyDataSetChanged();
            }

            @Override
            public void onClick(int position) {

            }
        });
        rvSubRegion.setAdapter(regionAdapter);
    }

    public void setupToolbar() {
        setSupportActionBar((Toolbar) findViewById(R.id.myToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public BaoCaoXuLyPresenter createPresenter() {
        return new BaoCaoXuLyPresenterImp(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAddImage:
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                //**These following line is the important one!
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), Key.SELECT_MULTI_PICTURES);
                break;
            case R.id.tvAddRegion:
                Log.d("TAG", "onClick: " + listRegionsFromApi.size());
                String[] regionName = new String[listRegionsFromApi.size()];
                for (int i = 0; i < listRegionsFromApi.size(); i++) {
                    regionName[i] = listRegionsFromApi.get(i).getName();
                }
                Log.d("TAG", "onClick: " + regionName.length);
                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setTitle("Chọn loại khu vực")
                        .setMultiChoiceItems(regionName, null, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                if (isChecked) {
                                    if (!listRegions.contains(listRegionsFromApi.get(which))) {
                                        listRegions.add(listRegionsFromApi.get(which));
                                        regionAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        });
                builder.create().show();
                break;
            case R.id.tvDeadLine:

                Helper.showDataPickerDialog(this, new Helper.OnDateSetListener() {
                    @Override
                    public void onDateSet(Calendar c) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        tvTime = sdf.format(c.getTime());
                        Log.d("TAG", "tv TIme lan 1: " + tvTime);
                        Helper.showTimePickerDialog(BaoCaoXuLyActivity.this, new Helper.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(Calendar c) {
                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                                tvDeadLine.setText(tvDeadLine.getText().toString() + " " + sdf.format(c.getTime()));
                                tvTime = tvTime + " " + sdf.format(c.getTime());
                                Log.d("TAG", "tv TIme lan 2: " + tvTime);
                                tvDeadLine.setText(tvTime);
                            }
                        });
                    }
                });

            case R.id.btnConfirm:
                if (!checkEmpty()) {
//@Multipart
//    @POST("api/v1/checklist/create-report")
//    Observable<JsonObject> createReport(@Header("Authorization") String token, @Part ArrayList<MultipartBody.Part> part,
//                                        @Part("desc") RequestBody desc,
//                                        @Part("recommend") RequestBody recommend,
//                                        @Part("deadline") RequestBody deadline,
//                                        @Part("checklist_id") RequestBody id,
//                                        @Part("quarters") ArrayList<String> quarters);
                    String desc = tvDesc.getText().toString();
                    String recommend = tvRecommend.getText().toString();
                    String deadLine = tvDeadLine.getText().toString();
                    String id = currentCheckList.getId().toString();
                    ArrayList<String> regionId = new ArrayList<>();
                    for(SubRegion subRegion : listRegions){
                        regionId.add(subRegion.getId().toString());
                    }

                    getPresenter().createReport(filePaths,desc,recommend,deadLine,id,regionId);
                }
                break;
        }

    }

    public boolean checkEmpty() {
        if (!TextUtils.isEmpty(edtDesc.getText().toString()) && !TextUtils.isEmpty(tvDeadLine.getText().toString()))
            return false;
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Key.SELECT_MULTI_PICTURES) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getClipData() != null) {
                    int count = data.getClipData().getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                    for (int i = 0; i < count; i++) {
                        Uri imageUri = data.getClipData().getItemAt(i).getUri();
                        Log.d("uri", "uri" + imageUri.toString());
                        listImageUri.add(imageUri.toString());
                        //do something with the image (save it to some directory or whatever you need to do with it here)

                    }
                    //get image path
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    for(int j=0; j<listImageUri.size();j++){
                        Cursor cursor = getContentResolver().query(Uri.parse(listImageUri.get(j)),
                                filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        filePaths.add(cursor.getString(columnIndex));
                        Log.d("do", "onActivityResult: " + filePaths.get(j));
                        cursor.close();
                    }



                    //adapter
                    imageAdapter = new ImageAdapter(this, listImageUri);
                    imageAdapter.setOnClickListener(new ImageAdapter.OnClickListener() {
                        @Override
                        public void onClickDelete(int position) {
                            listImageUri.remove(position);
                            imageAdapter.notifyDataSetChanged();
                        }
                    });
                    rvImage.setAdapter(imageAdapter);

                } else if (data.getData() != null) {
                    String imagePath = data.getData().getPath();
                    //do something with the image (save it to some directory or whatever you need to do with it here)
                }
            }
        }
    }

    @Override
    public void notifyRegionData() {

    }

    @Override
    public Context gContext() {
        finish();
        return this;
    }
}
