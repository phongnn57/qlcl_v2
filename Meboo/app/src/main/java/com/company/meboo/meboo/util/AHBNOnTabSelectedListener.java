package com.company.meboo.meboo.util;

import android.support.v4.view.ViewPager;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;

public class AHBNOnTabSelectedListener implements AHBottomNavigation.OnTabSelectedListener {
    ViewPager viewPager;
    AHBottomNavigation bottomNavigation;

    public AHBNOnTabSelectedListener(ViewPager viewPager, AHBottomNavigation bottomNavigation) {
        this.viewPager = viewPager;
        this.bottomNavigation = bottomNavigation;
    }

    @Override
    public boolean onTabSelected(int position, boolean wasSelected) {
        switch (position) {
            case 0:
                viewPager.setCurrentItem(0);
                bottomNavigation.setNotification("", 0);
                break;
            case 1:
                viewPager.setCurrentItem(1);
                bottomNavigation.setNotification("", 0);
                break;
            case 2:
                viewPager.setCurrentItem(2);
                bottomNavigation.setNotification("", 0);
                break;
            case 3:
                viewPager.setCurrentItem(3);
                bottomNavigation.setNotification("", 0);
                break;
            case 4:
                viewPager.setCurrentItem(4);
                bottomNavigation.setNotification("", 0);
                break;
        }
        return true;
    }
}
