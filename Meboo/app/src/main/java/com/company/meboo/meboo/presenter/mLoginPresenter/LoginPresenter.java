package com.company.meboo.meboo.presenter.mLoginPresenter;

import com.company.meboo.meboo.base.BasePresenter;

public interface LoginPresenter extends BasePresenter {
    void onImvDeleteClick(int isAccount);
    void login(String name, String password);
}
