package com.company.meboo.meboo.presenter.mMainPresenter;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.ui.activity.mMainActivity.MainView;

public class MainPresenterImp extends BasePresenterImpl<MainView> implements MainPresenter {
    public MainPresenterImp(MainView view) {
        super(view);
    }
}
