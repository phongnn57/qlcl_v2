package com.company.meboo.meboo.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.company.meboo.meboo.R;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.network.RetrofitClient;

import java.util.ArrayList;

public class QuanLyKhuVucAdapter extends RecyclerView.Adapter<QuanLyKhuVucAdapter.ViewHolder>{
    Context context;
    ArrayList<SubRegion> list;
    OnClickListener onClickListener;

    public QuanLyKhuVucAdapter(Context context, ArrayList<SubRegion> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.quan_ly_khu_vuc_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(list.get(position).getName());
        holder.tvCount.setText(list.get(position).getCount() + " Khu vực còn");
        Glide.with(context).load(ApiUtils.BASE_URL + list.get(position).getImage()).into(holder.imvImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        ImageView imvImage;
        TextView tvName, tvCount;
        public ViewHolder(View itemView) {
            super(itemView);
            imvImage = itemView.findViewById(R.id.imvImage);
            tvName = itemView.findViewById(R.id.tvName);
            tvCount = itemView.findViewById(R.id.tvCount);
        }
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener{
        void onClick(int position);
    }
}
