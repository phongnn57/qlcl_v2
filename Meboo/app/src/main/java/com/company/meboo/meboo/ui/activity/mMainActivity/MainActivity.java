package com.company.meboo.meboo.ui.activity.mMainActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.widget.Toast;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.ui.fragment.CaiDatFragment.FmCaiDat;
import com.company.meboo.meboo.ui.fragment.CanXuLyFragment.FmCanXuLy;
import com.company.meboo.meboo.ui.fragment.ThongBaoFragment.FmThongBao;
import com.company.meboo.meboo.ui.fragment.BaoCaoFragment.FmBaoCao;
import com.company.meboo.meboo.ui.fragment.CheckListFragment.FmCheckList;
import com.company.meboo.meboo.presenter.mMainPresenter.MainPresenter;
import com.company.meboo.meboo.presenter.mMainPresenter.MainPresenterImp;
import com.company.meboo.meboo.ui.adapter.ViewPagerAdapter;
import com.company.meboo.meboo.util.AHBNOnTabSelectedListener;
import com.company.meboo.meboo.util.Key;
import com.company.meboo.meboo.util.ViewPagerChangePageListener;

import java.util.ArrayList;

public class MainActivity extends BaseActivity<MainPresenter> implements MainView {
    AHBottomNavigation bottomNavigation;
    FragmentManager fm = getSupportFragmentManager();
    FragmentTransaction fragmentTransaction;
    ViewPager viewPager;
    ArrayList<Fragment> listFrags;

    @Override
    public int getContentViewId() {
        return R.layout.activity_main;
    }

    @Override
    public void initializeComponents() {
        init();
        setupBottomNav();
        setupViewPager();
        bottomNavigation.setNotification("1", 2);
        String khuVuc = getIntent().getStringExtra(Key.CODE_KHU_VUC);
        Toast.makeText(this, khuVuc+"", Toast.LENGTH_SHORT).show();
    }

    @Override
    public MainPresenter createPresenter() {
        return new MainPresenterImp(this);
    }

    public void init() {
        viewPager = findViewById(R.id.viewPager);
        bottomNavigation = findViewById(R.id.bottom_navigation);
        fragmentTransaction = fm.beginTransaction();
    }

    //tao bottom navigation
    public void setupBottomNav() {
        //tao item
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(getString(R.string.check_list), R.drawable.icon_check_list_24dp);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(getString(R.string.can_xu_ly), R.drawable.icon_process_24dp);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(getString(R.string.bao_cao), R.drawable.icon_report_24dp);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(getString(R.string.thong_bao), R.drawable.icon_notify_24dp);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(getString(R.string.cai_dat), R.drawable.icon_settings_24dp);
        bottomNavigation.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        //them vao nav
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.addItem(item4);
        bottomNavigation.addItem(item5);
        //bat su kien
        bottomNavigation.setOnTabSelectedListener(new AHBNOnTabSelectedListener(viewPager, bottomNavigation));
        bottomNavigation.setCurrentItem(0);
    }

    //tao view pager
    public void setupViewPager() {
        //tao 5 fragment ung voi 5 item bottom navigation
        listFrags = new ArrayList<>();
        listFrags.add(new FmCheckList());
        listFrags.add(new FmCanXuLy());
        listFrags.add(new FmBaoCao());
        listFrags.add(new FmThongBao());
        listFrags.add(new FmCaiDat());
        //tao adapter
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(fm, listFrags);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(0);

        //chuyen bottom nav theo view pager
        viewPager.addOnPageChangeListener(new ViewPagerChangePageListener(bottomNavigation));
    }
}
