package com.company.meboo.meboo.ui.fragment.ThongBaoFragment;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mThongBaoPresenter.ThongBaoPresenter;

public interface ThongBaoView extends BaseView<ThongBaoPresenter>{
    Context gContext();

    void notifyData();
}
