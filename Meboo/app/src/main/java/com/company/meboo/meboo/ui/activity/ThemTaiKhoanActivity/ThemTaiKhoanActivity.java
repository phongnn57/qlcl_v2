package com.company.meboo.meboo.ui.activity.ThemTaiKhoanActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.preference.PreferenceActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.model.model.object.SubQuarter;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.presenter.mThemTaiKhoanPresenter.ThemTaiKhoanPresenter;
import com.company.meboo.meboo.presenter.mThemTaiKhoanPresenter.ThemTaiKhoanPresenterImp;
import com.company.meboo.meboo.ui.adapter.RegionAdapter;
import com.company.meboo.meboo.util.Helper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class ThemTaiKhoanActivity extends BaseActivity<ThemTaiKhoanPresenter> implements ThemTaiKhoanView, View.OnClickListener {
    TextView tvType, tvQuarter, tvBirthday, tvChoose;
    EditText edtUserName, edtPassword, edtRePassword, edtName, edtPhone, edtEmail;
    RecyclerView rvRegion;
    Button btnConfirm;

    String[] typeAccount = {"Admin", "Kiểm soát chất lượng", "Giám đốc", "Trưởng bộ phận", "Nhân viên"};


    ArrayList<SubRegion> listRegionFromApi = new ArrayList<>();
    ArrayList<SubRegion> listRegion = new ArrayList<>();

    String[] quarterName = new String[6];
    ArrayList<SubQuarter> listQuarterFromApi = new ArrayList<>();

    RegionAdapter regionAdapter;
    int type = -1;
    int quarterId = -1;

    @Override
    public int getContentViewId() {
        return R.layout.activity_them_tai_khoan;
    }

    @Override
    public void initializeComponents() {
        setSupportActionBar((Toolbar) findViewById(R.id.myToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        init();
        setupRecyclerView();
    }

    @Override
    public ThemTaiKhoanPresenter createPresenter() {
        return new ThemTaiKhoanPresenterImp(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    public void init() {
        tvType = findViewById(R.id.tvType);
        tvQuarter = findViewById(R.id.tvQuarter);
        tvBirthday = findViewById(R.id.tvBirthday);
        edtUserName = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPassword);
        edtRePassword = findViewById(R.id.edtRePassword);
        edtName = findViewById(R.id.edtName);
        edtPhone = findViewById(R.id.edtPhone);
        edtEmail = findViewById(R.id.edtEmail);
        rvRegion = findViewById(R.id.rvRegion);
        btnConfirm = findViewById(R.id.btnConfirm);
        tvChoose = findViewById(R.id.tvChoose);

        tvType.setOnClickListener(this);
        tvQuarter.setOnClickListener(this);
        tvBirthday.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        tvChoose.setOnClickListener(this);
    }

    public void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvRegion.setLayoutManager(linearLayoutManager);
        regionAdapter = new RegionAdapter(this, listRegion, R.layout.region_item);
        regionAdapter.setOnItemClickListener(new RegionAdapter.OnItemClickListener() {
            @Override
            public void onClickDelete(int position) {
                listRegion.remove(position);
                regionAdapter.notifyDataSetChanged();
            }

            @Override
            public void onClick(int position) {

            }
        });
        rvRegion.setAdapter(regionAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvType:
                createTypeAccountDialog();
                break;
            case R.id.tvQuarter:
                chooseQuarter();
                break;
            case R.id.tvBirthday:
                createDatePickerDialog();
                break;
            case R.id.tvChoose:
                chooseRegion();
                break;
            case R.id.btnConfirm:
                createDataAccount();
                break;
        }
    }

    private void createDataAccount() {
        if (checkEmpty()) {
            Toast.makeText(this, "Vui lòng nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
        } else if (!checkPassword()) {
            Toast.makeText(this, "Mật khẩu không trùng khớp", Toast.LENGTH_SHORT).show();
        } else {
            String userName = edtUserName.getText().toString();
            String password = edtPassword.getText().toString();
            //type
            //quarter id
            //list regions
            String name = edtName.getText().toString();
            String phone = edtPhone.getText().toString();
            String email = edtEmail.getText().toString();
            String birthday = tvBirthday.getText().toString();
            getPresenter().createAccount(userName, password, type, quarterId, listRegion, name, phone, email, birthday);
        }
    }

    public boolean checkPassword() {
        if (!edtPassword.getText().toString().equals(edtRePassword.getText().toString()))
            return false;
        return true;
    }

    public boolean checkEmpty() {
        if (type == -1 || listRegion.size() < 0 || quarterId == -1 || TextUtils.isEmpty(edtUserName.getText().toString()) ||
                TextUtils.isEmpty(edtName.getText().toString()) || TextUtils.isEmpty(edtPhone.getText().toString())
                || TextUtils.isEmpty(tvBirthday.getText().toString()) || TextUtils.isEmpty(edtEmail.getText().toString())
                || TextUtils.isEmpty(edtPassword.getText().toString()) || TextUtils.isEmpty(edtRePassword.getText().toString()))
            return true;
        return false;
    }

    private void chooseQuarter() {
        listQuarterFromApi = getPresenter().getListQuarter();
        for (int i = 0; i < listQuarterFromApi.size(); i++) {
            quarterName[i] = listQuarterFromApi.get(i).getName().toString();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setItems(quarterName, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        quarterId = which;
                        tvQuarter.setText(quarterName[which]);
                    }
                });
        builder.create().show();

    }

    private void createDatePickerDialog() {
        Helper.showDataPickerDialog(this, new Helper.OnDateSetListener() {
            @Override
            public void onDateSet(Calendar c) {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                tvBirthday.setText(sdf.format(c.getTime()));
            }
        });
    }

    private void chooseRegion() {
        listRegionFromApi = getPresenter().getListRegion();
        String[] regionName = new String[listRegionFromApi.size()];
        for (int i = 0; i < listRegionFromApi.size(); i++) {
            regionName[i] = listRegionFromApi.get(i).getName().toString();
        }

        if (type == -1) {
            Helper.showMessage("Vui lòng chọn tài khoản trước", this);
        } else if (type == 0) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Chọn loại khu vực")
                    .setMultiChoiceItems(regionName, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                            if (isChecked) {
                                if (!listRegion.contains(listRegionFromApi.get(which))) {
                                    listRegion.add(listRegionFromApi.get(which));
                                    regionAdapter.notifyDataSetChanged();
                                }
                            }
                        }
                    });
            builder.create().show();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this)
                    .setTitle("Chọn loại khu vực")
                    .setItems(regionName, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            listRegion.clear();
                            listRegion.add(listRegionFromApi.get(which));
                            regionAdapter.notifyDataSetChanged();
                        }
                    });
            builder.create().show();
        }
    }

    public void createTypeAccountDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Chọn loại tài khoản")
                .setItems(typeAccount, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        tvType.setText(typeAccount[which]);
                        type = which+1;
                        Log.d("abc",which+"");
                        dialog.dismiss();
                    }
                })
                .setCancelable(false);
        builder.create().show();
    }

    @Override
    public void notifyRegionData() {
        Log.d("Add", getPresenter().getListRegion().size() + "");
        regionAdapter.notifyDataSetChanged();
    }

    @Override
    public void notifyQuarterData() {

    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public Context gContext() {
        return this;
    }
}
