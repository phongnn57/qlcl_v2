package com.company.meboo.meboo.model;

import com.company.meboo.meboo.model.model.object.Account;
import com.company.meboo.meboo.model.model.object.KhuVuc;

import java.util.ArrayList;

public class KhuVucModel {
    ArrayList<KhuVuc> listKhuVuc;

    public KhuVucModel() {
        if(listKhuVuc == null){
            listKhuVuc = new ArrayList<>();
            listKhuVuc.add(new KhuVuc(0,"Hà Nội"));
            listKhuVuc.add(new KhuVuc(1,"Sài Gòn"));
            listKhuVuc.add(new KhuVuc(2,"TP Hồ Chí Minh"));
            listKhuVuc.add(new KhuVuc(3,"Đà Nẵng"));
            listKhuVuc.add(new KhuVuc(4,"Nha Trang"));
        }
    }

    public ArrayList<KhuVuc> getListKhuVuc(){
        return listKhuVuc;
    }
}
