package com.company.meboo.meboo.ui.activity.mChiTietYeuCauGiaHan;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.model.model.object.DataReport;
import com.company.meboo.meboo.presenter.mChiTietYeuCauGiaHanPresenter.ChiTietYeuCauGiaHanPresenter;

public interface ChiTietYeuCauGiaHanView extends BaseView<ChiTietYeuCauGiaHanPresenter>{
    Context gContext();

    void setData(DataReport data);
}
