package com.company.meboo.meboo.ui.activity.mQuanLyTaiKhoanActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.presenter.mQuanLyTaiKhoanPresenter.QuanLyTaiKhoanPresenter;
import com.company.meboo.meboo.presenter.mQuanLyTaiKhoanPresenter.QuanLyTaiKhoanPresenterImp;
import com.company.meboo.meboo.presenter.mThemTaiKhoanPresenter.ThemTaiKhoanPresenter;
import com.company.meboo.meboo.ui.activity.ThemTaiKhoanActivity.ThemTaiKhoanActivity;
import com.company.meboo.meboo.ui.activity.mMainActivity.MainActivity;
import com.company.meboo.meboo.ui.adapter.AccountAdapter;
import com.company.meboo.meboo.util.EndlessRecyclerViewScrollListener;

public class QuanLyTaiKhoanActivity extends BaseActivity<QuanLyTaiKhoanPresenter> implements QuanLyTaiKhoanView, View.OnClickListener {
    RecyclerView rvAccount;
    AccountAdapter accountAdapter;
    ImageView imvAddAccount;

    @Override
    public int getContentViewId() {
        return R.layout.activity_quan_ly_tai_khoan;
    }

    @Override
    public void initializeComponents() {
        imvAddAccount = findViewById(R.id.imvAddAccount);
        setSupportActionBar((Toolbar) findViewById(R.id.myToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rvAccount = findViewById(R.id.rvAccount);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rvAccount.setLayoutManager(linearLayoutManager);
        accountAdapter = new AccountAdapter(this, getPresenter().getAccountList());
        accountAdapter.setOnItemClickListener(new AccountAdapter.OnItemClickListener() {
            @Override
            public void onClick(final int position) {
                AlertDialog builder = new AlertDialog.Builder(QuanLyTaiKhoanActivity.this)
                        .setTitle("Xóa tài khoản")
                        .setMessage("Bạn có chắc không?")
                        .setIcon(R.drawable.icon_delete_2)
                        .setPositiveButton("CÓ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                accountAdapter.remove(position);
                                dialog.cancel();
                            }
                        })
                        .setNegativeButton("KHÔNG", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .create();
                builder.show();
            }
        });
        rvAccount.setAdapter(accountAdapter);

        imvAddAccount.setOnClickListener(this);
    }

    @Override
    public QuanLyTaiKhoanPresenter createPresenter() {
        return new QuanLyTaiKhoanPresenterImp(this);
    }

    @Override
    public void notifyData() {
        accountAdapter.notifyDataSetChanged();
    }

    @Override
    public Context gContext() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(QuanLyTaiKhoanActivity.this, ThemTaiKhoanActivity.class);
        startActivity(intent);
    }
}
