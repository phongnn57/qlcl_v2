package com.company.meboo.meboo.presenter.mBaoCaoPresenter;

import android.util.Log;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.QuarterReport;
import com.company.meboo.meboo.model.model.object.SubQuarter;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.network.DataResponse;
import com.company.meboo.meboo.network.Response;
import com.company.meboo.meboo.ui.fragment.BaoCaoFragment.BaoCaoView;
import com.company.meboo.meboo.util.PrefUtil;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BaoCaoPresenterImp extends BasePresenterImpl<BaoCaoView> implements BaoCaoPresenter {
    ArrayList<QuarterReport> arrayReport = new ArrayList<>();
    public BaoCaoPresenterImp(BaoCaoView view) {
        super(view);
    }


    @Override
    public void getData(String startTime, String endTime) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        Log.d("FmBaoCao",token);
        ApiUtils.getQlclService().getQuarterReport(token,startTime,endTime).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<List<QuarterReport>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<List<QuarterReport>> listResponse) {
                        arrayReport.clear();
                        arrayReport.addAll(listResponse.getData());
                        Log.d("FmBaoCao","size in imp:" + arrayReport.size()+"");
                        getView().notifyData();
                    }
                });
    }

    @Override
    public ArrayList<QuarterReport> getQuarterReport() {
        return arrayReport;
    }
}
