package com.company.meboo.meboo.ui.activity.mChiTietYeuCauGiaHan;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.model.model.object.DataReport;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.presenter.mChiTietYeuCauGiaHanPresenter.ChiTietYeuCauGiaHanPresenter;
import com.company.meboo.meboo.presenter.mChiTietYeuCauGiaHanPresenter.ChiTietYeuCauGiaHanPresenterImp;
import com.company.meboo.meboo.util.Helper;
import com.company.meboo.meboo.util.Key;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ChiTietYeuCauGiaHanActivity extends BaseActivity<ChiTietYeuCauGiaHanPresenter> implements ChiTietYeuCauGiaHanView, View.OnClickListener {
    TextView tvNameChecklist, tvQuarterChecklist, tvDescChecklist, tvDesc, tvDeadLine, tvExtendTime, tvDistanceDate, tvReason;
    LinearLayout ln_extend;
    EditText edtRejectRequest;
    TextView tvDialogDeadLine;
    ImageView imvProve;
    TextView btnReject, btnConfirm;
    DataReport data;
    int id;

    @Override
    public int getContentViewId() {
        return R.layout.activity_chi_tiet_yeu_cau_gia_han;
    }

    @Override
    public void initializeComponents() {
        setSupportActionBar((Toolbar) findViewById(R.id.myToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        tvNameChecklist = findViewById(R.id.tvNameChecklist);
        imvProve = findViewById(R.id.imvProve);
        btnReject = findViewById(R.id.btnReject);
        btnConfirm = findViewById(R.id.btnConfirm);
        tvQuarterChecklist = findViewById(R.id.tvQuarterChecklist);
        tvDescChecklist = findViewById(R.id.tvDescChecklist);
        tvDesc = findViewById(R.id.tvDesc);
        tvDeadLine = findViewById(R.id.tvDeadLine);
        tvExtendTime = findViewById(R.id.tvExtendTime);
        tvDistanceDate = findViewById(R.id.tvDistanceDate);
        tvReason = findViewById(R.id.tvReason);
        ln_extend = findViewById(R.id.ln_extend);
        edtRejectRequest = findViewById(R.id.edtRejectRequest);

        btnReject.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);


        id = getIntent().getIntExtra(Key.ID, 0);
        if (id != 0) {
            getPresenter().callApiReportExtended(id);
        }
    }

    @Override
    public ChiTietYeuCauGiaHanPresenter createPresenter() {
        return new ChiTietYeuCauGiaHanPresenterImp(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Context gContext() {
        return this;
    }

    @Override
    public void setData(DataReport data) {
        this.data = data;

        if (data.getChecklist() != null) {
            tvNameChecklist.setText(data.getChecklist().getName() + "");
            tvQuarterChecklist.setText(data.getChecklist().getQuarterName() + "");
            tvDescChecklist.setText(data.getChecklist().getDesc() + "");
        }

        if (data.getImages() != null)
            Picasso.with(this).load(ApiUtils.BASE_URL + data.getImages().get(0)).into(imvProve);
        else imvProve.setVisibility(View.GONE);

        tvDesc.setText(data.getDesc() + "");
        tvDeadLine.setText(data.getDeadline() + "");

        tvExtendTime.setText(data.getExtendTime() + "");


        try {
            SimpleDateFormat sdfOld = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdfNew = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");

            Date deadLine = sdfOld.parse(data.getDeadline());
            Date newDateLine = sdfOld.parse(data.getExtendTime());

            Date deadLine2 = sdfNew.parse(sdfNew.format(deadLine));
            Date newDateLine2 = sdfNew.parse(sdfNew.format(newDateLine));

            tvDistanceDate.setText("Thời gian gia hạn tăng thêm " + Helper.getTime(deadLine2, newDateLine2));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        tvReason.setText(data.getReasonExtend() + "");

        if (data.getIsAccept() == 1) ln_extend.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReject:
                if (!TextUtils.isEmpty(edtRejectRequest.getText().toString())) {
                    getPresenter().reject(id, edtRejectRequest.getText().toString());
                    finish();
                }
                break;
            case R.id.btnConfirm:
                View view = getLayoutInflater().inflate(R.layout.dialog_chon_dead_line,null);
                tvDialogDeadLine = view.findViewById(R.id.tvDialogDeadLine);
                tvDialogDeadLine.setText(tvExtendTime.getText());
                tvDialogDeadLine.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Helper.showDataPickerDialog(ChiTietYeuCauGiaHanActivity.this, new Helper.OnDateSetListener() {
                            @Override
                            public void onDateSet(Calendar c) {
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                                tvDialogDeadLine.setText(sdf.format(c.getTime()));
                                Helper.showTimePickerDialog(ChiTietYeuCauGiaHanActivity.this, new Helper.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(Calendar c) {
                                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                                        tvDialogDeadLine.setText(tvDialogDeadLine.getText() + " " + sdf.format(c.getTime()));
                                    }
                                });
                            }
                        });
                    }
                });

                AlertDialog.Builder builder = new AlertDialog.Builder(this)
                        .setView(view)
                        .setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SimpleDateFormat sdfOld = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                                try {
                                    Date dateOld = sdfOld.parse(tvExtendTime.getText().toString());
                                    String date = sdf.format(dateOld);
                                    getPresenter().accept(id, date);
                                    finish();
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                            }
                        })
                        .setNegativeButton("Thoát", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setCancelable(false);
                builder.create().show();




                break;
        }
    }
}
