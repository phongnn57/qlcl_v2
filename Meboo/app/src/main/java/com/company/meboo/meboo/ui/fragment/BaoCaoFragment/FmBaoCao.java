package com.company.meboo.meboo.ui.fragment.BaoCaoFragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseFragment;
import com.company.meboo.meboo.model.model.object.QuarterReport;
import com.company.meboo.meboo.presenter.mBaoCaoPresenter.BaoCaoPresenter;
import com.company.meboo.meboo.presenter.mBaoCaoPresenter.BaoCaoPresenterImp;
import com.company.meboo.meboo.ui.adapter.BaoCaoAdapter;
import com.company.meboo.meboo.util.Helper;
import com.company.meboo.meboo.util.Key;
import com.company.meboo.meboo.util.PrefUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class FmBaoCao extends BaseFragment<BaoCaoPresenter> implements BaoCaoView, View.OnClickListener {
    ImageView imvFilter;
    RecyclerView rvReport;
    BaoCaoAdapter baoCaoAdapter;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    ArrayList<QuarterReport> arrayReport = new ArrayList<>();
    Calendar cal = Calendar.getInstance();
    String TAG = "FmBaoCao";

    @Override
    public int getContentViewId() {
        return R.layout.fm_bao_cao;
    }

    @Override
    public void initializeComponents(View view) {
        init(view);
        getPresenter().getData(PrefUtil.getString(getActivity(), Key.START_TIME, sdf.format(cal.getTime())),
                PrefUtil.getString(getActivity(), Key.END_TIME, sdf.format(cal.getTime())));
        arrayReport = getPresenter().getQuarterReport();
        baoCaoAdapter = new BaoCaoAdapter(getActivity(), arrayReport);
        rvReport.setAdapter(baoCaoAdapter);
    }

    private void init(View view) {
        imvFilter = view.findViewById(R.id.imvFilter);
        imvFilter.setOnClickListener(this);
        rvReport = view.findViewById(R.id.rvReport);
        rvReport.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
    }

    @Override
    public BaoCaoPresenter createPresenter() {
        return new BaoCaoPresenterImp(this);
    }

    @Override
    public Context gContext() {
        return getActivity();
    }

    @Override
    public void notifyData() {
        Log.d(TAG, "Size in notify" + arrayReport.size());
        baoCaoAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View v) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_filter, null);
        final TextView tvTimeRange = view.findViewById(R.id.tvTimeRange);
        final TextView tvTimeStart = view.findViewById(R.id.tvTimeStart);
        final TextView tvTimeEnd = view.findViewById(R.id.tvTimeEnd);
        Button btnCancel = view.findViewById(R.id.btnCancel);
        Button btnFilter = view.findViewById(R.id.btnFilter);

        tvTimeStart.setText(PrefUtil.getString(getActivity(), Key.START_TIME, sdf.format(cal.getTime())));
        tvTimeEnd.setText(PrefUtil.getString(getActivity(), Key.END_TIME, sdf.format(cal.getTime())));
        tvTimeRange.setText(PrefUtil.getString(getActivity(), Key.RANGE_TIME, "Hôm nay"));

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setView(view);
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPresenter().getData(tvTimeStart.getText().toString(), tvTimeEnd.getText().toString());
                PrefUtil.saveString(getActivity(), tvTimeStart.getText().toString(), Key.START_TIME);
                PrefUtil.saveString(getActivity(), tvTimeEnd.getText().toString(), Key.END_TIME);
                PrefUtil.saveString(getActivity(), tvTimeRange.getText().toString(), Key.RANGE_TIME);
                alertDialog.dismiss();
            }
        });

        tvTimeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.showDataPickerDialog(getActivity(), new Helper.OnDateSetListener() {
                    @Override
                    public void onDateSet(Calendar c) {
                        tvTimeStart.setText(sdf.format(c.getTime()));
                        tvTimeRange.setText("Tự chọn");
                    }
                });
            }
        });

        tvTimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Helper.showDataPickerDialog(getActivity(), new Helper.OnDateSetListener() {
                    @Override
                    public void onDateSet(Calendar c) {
                        tvTimeEnd.setText(sdf.format(c.getTime()));
                        tvTimeRange.setText("Tự chọn");
                    }
                });
            }
        });

        tvTimeRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String[] items = {
                        "Hôm qua", "Hôm nay", "Tuần này", "7 ngày qua", "Tuần trước", "Tháng này", "30 ngày qua", "Tháng trước"
                };
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                        .setItems(items, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                tvTimeRange.setText(items[which]);
                                refreshTextView(tvTimeStart, tvTimeEnd, which);
                            }
                        });
                builder.create().show();
            }
        });

//        builder.create().show();


    }

    private void refreshTextView(TextView tvTimeStart, TextView tvTimeEnd, int which) {
        switch (which) {
            case 0:
                //hôm qua
                cal.setTime(new Date());
                cal.add(Calendar.DAY_OF_YEAR, -1);
                tvTimeStart.setText(sdf.format(cal.getTime()));
                tvTimeEnd.setText(sdf.format(cal.getTime()));
                break;
            case 1:
                //hôm nay
                cal.setTime(new Date());
                tvTimeStart.setText(sdf.format(cal.getTime()));
                tvTimeEnd.setText(sdf.format(cal.getTime()));
                break;
            case 2:
                //tuần này
                cal.setTime(new Date());
                tvTimeEnd.setText(sdf.format(cal.getTime()));
                cal.setFirstDayOfWeek(Calendar.MONDAY);
                cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
                tvTimeStart.setText(sdf.format(cal.getTime()));
                break;
            case 3:
                //7 ngày trước
                cal.setTime(new Date());
                tvTimeEnd.setText(sdf.format(cal.getTime()));
                cal.add(Calendar.DAY_OF_YEAR, -7);
                tvTimeStart.setText(sdf.format(cal.getTime()));
                break;
            case 4:
                //tuần trước
                cal.setTime(new Date());
                cal.setFirstDayOfWeek(Calendar.MONDAY);
                cal.set(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek());
                cal.add(Calendar.DAY_OF_WEEK, -1);
                tvTimeEnd.setText(sdf.format(cal.getTime()));
                cal.add(Calendar.DAY_OF_MONTH, -6);
                tvTimeStart.setText(sdf.format(cal.getTime()));
                break;
            case 5:
                //tháng này
                cal.setTime(new Date());
                tvTimeEnd.setText(sdf.format(cal.getTime()));
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
                tvTimeStart.setText(sdf.format(cal.getTime()));
                break;
            case 6:
                //30 ngày trước
                cal.setTime(new Date());
                tvTimeEnd.setText(sdf.format(cal.getTime()));
                cal.add(Calendar.DAY_OF_YEAR, -30);
                tvTimeStart.setText(sdf.format(cal.getTime()));
                break;
            case 7:
                //tháng trước
                cal.setTime(new Date());
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
                cal.add(Calendar.DAY_OF_MONTH, -1);
                tvTimeEnd.setText(sdf.format(cal.getTime()));
                cal.set(Calendar.DAY_OF_MONTH, cal.getActualMinimum(Calendar.DAY_OF_MONTH));
                tvTimeStart.setText(sdf.format(cal.getTime()));
                break;
        }
    }
}
