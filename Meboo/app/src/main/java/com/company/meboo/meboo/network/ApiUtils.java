package com.company.meboo.meboo.network;

public class ApiUtils {
//    public static final String BASE_URL = "http://qlcl.fitme.vn";
    public static final String BASE_URL = "http://qlcl-live.fitme.vn";

    public static QlclService getQlclService() {
        return RetrofitClient.getClient(BASE_URL).create(QlclService.class);
    }
}
