package com.company.meboo.meboo.presenter.mCheckListPresenter;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.CheckList;

import java.util.ArrayList;

public interface CheckListPresenter extends BasePresenter{
    ArrayList<CheckList> getCheckList();
    void initCheckList();
    void loadNextData();
}
