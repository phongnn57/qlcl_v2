package com.company.meboo.meboo.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.model.model.object.CheckList;

import java.util.ArrayList;

public class CheckListDetailAdapter extends RecyclerView.Adapter<CheckListDetailAdapter.ViewHolder>{
    ArrayList<CheckList> list ;
    Context context;
    OnClickListener onClickListener;

    public CheckListDetailAdapter(Context context,ArrayList<CheckList> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.check_list_detail_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
//        if(list.get(position).getQuarter().equals("ANNB")){
            holder.workTypeQuarter.setText(list.get(position).getWorkType() + " (" + list.get(position).getQuarter() + ")");
            holder.desc.setText(list.get(position).getDesc());
            holder.lastCheck.setText("Kiểm tra lần cuối vào lúc: " + list.get(position).getLastCheck());

            holder.btnBad.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onBadClick(position);
                }
            });
            holder.btnGood.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickListener.onGoodClick(position);
                }
            });
//        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView workTypeQuarter, desc, lastCheck;
        Button btnGood, btnBad;
        public ViewHolder(View itemView) {
            super(itemView);
            workTypeQuarter = itemView.findViewById(R.id.workTypeQuarter);
            desc = itemView.findViewById(R.id.desc);
            lastCheck = itemView.findViewById(R.id.lastCheck);
            btnGood = itemView.findViewById(R.id.btnGood);
            btnBad = itemView.findViewById(R.id.btnBad);
        }
    }

    public void updateCheckList(ArrayList<CheckList> items) {
        list = items;
        notifyDataSetChanged();
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener{
        void onGoodClick(int position);
        void onBadClick(int position);
    }
}
