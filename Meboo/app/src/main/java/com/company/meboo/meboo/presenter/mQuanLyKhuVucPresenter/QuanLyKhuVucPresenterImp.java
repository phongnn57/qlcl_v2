package com.company.meboo.meboo.presenter.mQuanLyKhuVucPresenter;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.network.DataResponse;
import com.company.meboo.meboo.network.Response;
import com.company.meboo.meboo.ui.activity.mQuanLyKhuVuc.QuanLyKhuVucView;
import com.company.meboo.meboo.util.PrefUtil;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class QuanLyKhuVucPresenterImp extends BasePresenterImpl<QuanLyKhuVucView> implements QuanLyKhuVucPresenter {
    ArrayList<SubRegion> listRegions = new ArrayList<>();

    public QuanLyKhuVucPresenterImp(QuanLyKhuVucView view) {
        super(view);
        callApiListRegion();
    }

    @Override
    public ArrayList<SubRegion> getListRegion() {
        return listRegions;
    }

    public void callApiListRegion(){
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().getRegionList(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<DataResponse<SubRegion>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<DataResponse<SubRegion>> dataResponseResponse) {
                        listRegions.clear();
                        listRegions.addAll(dataResponseResponse.getData().getSub());
                        getView().notifyRegionData();
                    }
                });
    }
}
