package com.company.meboo.meboo.presenter.mBaoCaoXuLyPresenter;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.SubRegion;

import java.util.ArrayList;

public interface BaoCaoXuLyPresenter extends BasePresenter {
    ArrayList<SubRegion> getListRegion();

    void callApiListRegion();

    void createReport(ArrayList<String> filePaths,
                      String desc,
                      String recommend,
                      String deadline,
                      String id,
                      ArrayList<String> idQuarters);
}

