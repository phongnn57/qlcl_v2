package com.company.meboo.meboo.ui.fragment.CanXuLyFragment;

import android.view.View;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseFragment;
import com.company.meboo.meboo.presenter.mCanXuLyPresenter.CanXuLyPresenter;
import com.company.meboo.meboo.presenter.mCanXuLyPresenter.CanXuLyPresenterImp;

public class FmCanXuLy extends BaseFragment<CanXuLyPresenter> implements CanXuLyView{

    @Override
    public int getContentViewId() {
        return R.layout.fm_can_xu_ly;
    }

    @Override
    public void initializeComponents(View view) {

    }

    @Override
    public CanXuLyPresenter createPresenter() {
        return new CanXuLyPresenterImp(this);
    }
}
