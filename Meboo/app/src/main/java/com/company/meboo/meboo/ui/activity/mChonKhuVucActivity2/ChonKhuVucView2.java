package com.company.meboo.meboo.ui.activity.mChonKhuVucActivity2;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.model.model.object.KhuVuc;
import com.company.meboo.meboo.presenter.mChonKhuVucPresenter2.ChonKhuVucPresenter2;

public interface ChonKhuVucView2 extends BaseView<ChonKhuVucPresenter2>{
    void chonKhuVuc(KhuVuc khuVuc);
}
