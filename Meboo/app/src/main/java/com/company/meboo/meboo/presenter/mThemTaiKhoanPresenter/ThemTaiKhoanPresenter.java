package com.company.meboo.meboo.presenter.mThemTaiKhoanPresenter;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.SubQuarter;
import com.company.meboo.meboo.model.model.object.SubRegion;

import java.util.ArrayList;

public interface ThemTaiKhoanPresenter extends BasePresenter {
    void callApiListRegion();

    ArrayList<SubRegion> getListRegion();

    ArrayList<SubQuarter> getListQuarter();

    void createAccount(String userName, String password, int type, int quarterId, ArrayList<SubRegion> listRegions,
                       String name, String phone, String email, String birthday);
}
