package com.company.meboo.meboo.ui.activity.mChonKhuVucActivity;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mChonKhuVucPresenter.ChonKhuVucPresenter;

public interface ChonKhuVucView extends BaseView<ChonKhuVucPresenter>{

    void onClickContinue(String s);
}
