package com.company.meboo.meboo.ui.fragment.CheckListFragment;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mCheckListPresenter.CheckListPresenter;

public interface CheckListView extends BaseView<CheckListPresenter>{
    void notifyData();

    Context gContext();
}
