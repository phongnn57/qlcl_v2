package com.company.meboo.meboo.presenter.mChonKhuVucPresenter;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.KhuVucModel;
import com.company.meboo.meboo.ui.activity.mChonKhuVucActivity.ChonKhuVucView;

public class ChonKhuVucPresenterImp extends BasePresenterImpl<ChonKhuVucView> implements ChonKhuVucPresenter {
    KhuVucModel khuVucModel = new KhuVucModel();

    public ChonKhuVucPresenterImp(ChonKhuVucView view) {
        super(view);
    }

    @Override
    public void onClickContinue(String s) {
        getView().onClickContinue(s);
    }

}
