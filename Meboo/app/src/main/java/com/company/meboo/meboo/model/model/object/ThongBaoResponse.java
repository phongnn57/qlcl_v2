package com.company.meboo.meboo.model.model.object;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ThongBaoResponse {

@SerializedName("status")
@Expose
private Integer status;
@SerializedName("data")
@Expose
private List<ThongBaoDetail> data = null;

public Integer getStatus() {
return status;
}

public void setStatus(Integer status) {
this.status = status;
}

public List<ThongBaoDetail> getData() {
return data;
}

public void setData(List<ThongBaoDetail> data) {
this.data = data;
}

}