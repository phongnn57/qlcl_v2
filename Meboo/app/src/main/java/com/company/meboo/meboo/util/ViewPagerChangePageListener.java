package com.company.meboo.meboo.util;


import android.content.Context;
import android.support.v4.view.ViewPager;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;

public class ViewPagerChangePageListener implements ViewPager.OnPageChangeListener {
    AHBottomNavigation bottomNavigation;

    public ViewPagerChangePageListener(AHBottomNavigation bottomNavigation) {
        this.bottomNavigation = bottomNavigation;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        bottomNavigation.setCurrentItem(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
