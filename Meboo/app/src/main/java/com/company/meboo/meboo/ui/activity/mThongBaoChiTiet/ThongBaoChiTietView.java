package com.company.meboo.meboo.ui.activity.mThongBaoChiTiet;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.model.model.object.DataReportDetail;
import com.company.meboo.meboo.presenter.mThongBaoChiTietPresenter.ThongBaoChiTietPresenter;

public interface ThongBaoChiTietView extends BaseView<ThongBaoChiTietPresenter>{
    Context gContext();

    void setData(DataReportDetail dataReportDetail);
}
