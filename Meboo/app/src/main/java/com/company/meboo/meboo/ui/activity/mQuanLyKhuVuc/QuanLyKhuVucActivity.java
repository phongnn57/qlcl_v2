package com.company.meboo.meboo.ui.activity.mQuanLyKhuVuc;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.presenter.mQuanLyKhuVucPresenter.QuanLyKhuVucPresenter;
import com.company.meboo.meboo.presenter.mQuanLyKhuVucPresenter.QuanLyKhuVucPresenterImp;
import com.company.meboo.meboo.ui.activity.mThemKhuVucActivity.ThemKhuVucActivity;
import com.company.meboo.meboo.ui.adapter.QuanLyKhuVucAdapter;

import java.util.ArrayList;

public class QuanLyKhuVucActivity extends BaseActivity<QuanLyKhuVucPresenter> implements QuanLyKhuVucView, View.OnClickListener {
    RecyclerView rvQuanLyKhuVuc;
    QuanLyKhuVucAdapter adapter;
    ArrayList<SubRegion> listRegion = new ArrayList<>();
    ImageView imvAddRegion;

    @Override
    public int getContentViewId() {
        return R.layout.activity_quan_ly_khu_vuc;
    }

    @Override
    public void initializeComponents() {
        init();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rvQuanLyKhuVuc.setLayoutManager(linearLayoutManager);
        listRegion = getPresenter().getListRegion();
        adapter = new QuanLyKhuVucAdapter(this,listRegion);
        rvQuanLyKhuVuc.setAdapter(adapter);
    }

    public void init(){
        setSupportActionBar((Toolbar) findViewById(R.id.myToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        rvQuanLyKhuVuc= findViewById(R.id.rvQuanLyKhuVuc);
        imvAddRegion= findViewById(R.id.imvAddRegion);
        imvAddRegion.setOnClickListener(this);
    }

    @Override
    public QuanLyKhuVucPresenter createPresenter() {
        return new QuanLyKhuVucPresenterImp(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void notifyRegionData() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public Context gContext() {
        return this;
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(QuanLyKhuVucActivity.this, ThemKhuVucActivity.class);

        startActivity(intent);
    }
}
