package com.company.meboo.meboo.firebase;

import com.company.meboo.meboo.network.QlclService;
import com.company.meboo.meboo.network.RetrofitClient;

public class FCMApiUtils {
    public static final String BASE_URL = "https://fcm.googleapis.com/";

    public static FirebaseApiService getFCMCService() {
        return FCMRetrofitClient.getClient(BASE_URL).create(FirebaseApiService.class);
    }
}
