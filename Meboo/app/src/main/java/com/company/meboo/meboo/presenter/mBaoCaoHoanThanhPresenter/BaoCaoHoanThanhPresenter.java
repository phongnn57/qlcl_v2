package com.company.meboo.meboo.presenter.mBaoCaoHoanThanhPresenter;

import com.company.meboo.meboo.base.BasePresenter;

import java.util.ArrayList;

public interface BaoCaoHoanThanhPresenter extends BasePresenter {
    void sendCompleteReport(String desc, ArrayList<String> filePaths, int id);
}
