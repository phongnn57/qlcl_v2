package com.company.meboo.meboo.presenter.mThemKhuVucPresenter;

import android.graphics.Region;
import android.util.Log;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.network.DataResponse;
import com.company.meboo.meboo.network.Response;
import com.company.meboo.meboo.ui.activity.mThemKhuVucActivity.ThemKhuVucView;
import com.company.meboo.meboo.util.PrefUtil;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ThemKhuVucPresenterImp extends BasePresenterImpl<ThemKhuVucView> implements ThemKhuVucPresenter {
    ArrayList<SubRegion> listRegions = new ArrayList<>();

    public ThemKhuVucPresenterImp(ThemKhuVucView view) {
        super(view);
    }

    @Override
    public ArrayList<SubRegion> getListRegion() {
        return listRegions;
    }

    public void callApiListRegion() {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().getRegionList(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<DataResponse<SubRegion>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<DataResponse<SubRegion>> dataResponseResponse) {
                        listRegions.clear();
                        listRegions.addAll(dataResponseResponse.getData().getSub());
                        getView().notifyRegionData();
                    }
                });
    }

    @Override
    public void createRegion(int parentID, String name, String desc, String filePath) {
        String token = "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6Ly9xbGNsLmZpdG1lLnZuL2FwaS92MS9sb2dpbiIsImlhdCI6MTUzMTMwNjA3MiwiZXhwIjoxNTMxMzA5NjcyLCJuYmYiOjE1MzEzMDYwNzIsImp0aSI6IkxqNjdxQjJTbGM2d2JwR1UifQ.LVrUrpDwSP_8vy-7Sobp6s9WUfp_N77yKDcUcjNK6ec";
        RequestBody nameRb = RequestBody.create(MediaType.parse("text/plain"), name);
        RequestBody descRb = RequestBody.create(MediaType.parse("text/plain"), desc);
        RequestBody parentIdRb = RequestBody.create(MediaType.parse("text/plain"), parentID>0?String.valueOf(parentID):"");
        File file = new File(filePath);
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
        MultipartBody.Part image = MultipartBody.Part.createFormData("image", file.getName(), reqFile);
        ApiUtils.getQlclService().createRegion(token, parentIdRb,nameRb,descRb,image)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("dd",e.getMessage()+"");
                    }

                    @Override
                    public void onNext(Response response) {
                        getView().finishActivity();
                    }
                });
    }
}
