package com.company.meboo.meboo.ui.activity.mBaoCaoXuLy;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mBaoCaoXuLyPresenter.BaoCaoXuLyPresenter;

public interface BaoCaoXuLyView extends BaseView<BaoCaoXuLyPresenter>{
    void notifyRegionData();

    Context gContext();
}
