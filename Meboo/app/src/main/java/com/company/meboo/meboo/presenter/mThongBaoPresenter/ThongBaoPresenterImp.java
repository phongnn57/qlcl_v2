package com.company.meboo.meboo.presenter.mThongBaoPresenter;

import android.widget.Toast;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.ThongBaoDetail;
import com.company.meboo.meboo.model.model.object.ThongBaoResponse;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.ui.fragment.ThongBaoFragment.ThongBaoView;
import com.company.meboo.meboo.util.PrefUtil;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ThongBaoPresenterImp extends BasePresenterImpl<ThongBaoView> implements ThongBaoPresenter {
    ArrayList<ThongBaoDetail> arrayThongBao = new ArrayList<>();
    int last_id = 0;
    int old_last_id = -1;
    int limit = 10;

    public ThongBaoPresenterImp(ThongBaoView view) {
        super(view);
        callApiListNotification();
    }


    @Override
    public ArrayList<ThongBaoDetail> getListNotification() {
        return arrayThongBao;
    }

    @Override
    public void callApiListNotification() {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());

            ApiUtils.getQlclService().getListNotification(token,limit,last_id).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<ThongBaoResponse>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(ThongBaoResponse thongBaoResponse) {
                            if(thongBaoResponse.getStatus() == 1){
                                arrayThongBao.addAll(thongBaoResponse.getData());
                                if(thongBaoResponse.getData().size() > 0 ){
                                    old_last_id = last_id;
                                    last_id = arrayThongBao.get(arrayThongBao.size()-1).getId();
                                }
                                getView().notifyData();
                            }

                        }
                    });
    }

    @Override
    public void loadMore() {
        if (last_id == old_last_id){
            return;
        }else{
            callApiListNotification();
        }
    }

    @Override
    public void readAll() {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().readAll(token).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        Toast.makeText(getView().gContext(), "Đã đọc hết", Toast.LENGTH_SHORT).show();
                        arrayThongBao.clear();
                        last_id = 0;
                        old_last_id =-1;
                        callApiListNotification();
                    }
                });
    }

    @Override
    public void read(int id) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().readNotify(token,id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        arrayThongBao.clear();
                        last_id = 0;
                        old_last_id = -1;
                        callApiListNotification();
                    }
                });
    }
}
