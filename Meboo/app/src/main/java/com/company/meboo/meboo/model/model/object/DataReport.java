package com.company.meboo.meboo.model.model.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by VietVan on 25/05/2018.
 */

public class DataReport {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("images")
    @Expose
    private List<String> images = null;
    @SerializedName("desc")
    @Expose
    private String desc;
    @SerializedName("checklist_id")
    @Expose
    private Integer checklistId;
    @SerializedName("recommend")
    @Expose
    private String recommend;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("deadline")
    @Expose
    private String deadline;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("is_fixed")
    @Expose
    private Integer isFixed;
    @SerializedName("images_fixed")
    @Expose
    private List<String> imagesFixed = null;
    @SerializedName("result_fixed")
    @Expose
    private String resultFixed;
    @SerializedName("extend_time")
    @Expose
    private String extendTime;
    @SerializedName("reason_extend")
    @Expose
    private String reasonExtend;
    @SerializedName("is_accept")
    @Expose
    private Integer isAccept;
    @SerializedName("checklist")
    @Expose
    private ChecklistReport checklist;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Integer getChecklistId() {
        return checklistId;
    }

    public void setChecklistId(Integer checklistId) {
        this.checklistId = checklistId;
    }

    public String getRecommend() {
        return recommend;
    }

    public void setRecommend(String recommend) {
        this.recommend = recommend;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getIsFixed() {
        return isFixed;
    }

    public void setIsFixed(Integer isFixed) {
        this.isFixed = isFixed;
    }

    public List<String> getImagesFixed() {
        return imagesFixed;
    }

    public void setImagesFixed(List<String> imagesFixed) {
        this.imagesFixed = imagesFixed;
    }

    public String getResultFixed() {
        return resultFixed;
    }

    public void setResultFixed(String resultFixed) {
        this.resultFixed = resultFixed;
    }

    public String getExtendTime() {
        return extendTime;
    }

    public void setExtendTime(String extendTime) {
        this.extendTime = extendTime;
    }

    public String getReasonExtend() {
        return reasonExtend;
    }

    public void setReasonExtend(String reasonExtend) {
        this.reasonExtend = reasonExtend;
    }

    public Integer getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(Integer isAccept) {
        this.isAccept = isAccept;
    }

    public ChecklistReport getChecklist() {
        return checklist;
    }

    public void setChecklist(ChecklistReport checklist) {
        this.checklist = checklist;
    }
}
