package com.company.meboo.meboo.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.model.model.object.ThongBaoDetail;
import com.company.meboo.meboo.util.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ThongBaoAdapter extends RecyclerView.Adapter<ThongBaoAdapter.ViewHolder>{
    Context context;
    ArrayList<ThongBaoDetail> arrayThongBaoDetail;
    OnItemClickListener onItemClickListener;

    public ThongBaoAdapter(Context context, ArrayList<ThongBaoDetail> arrayThongBaoDetail) {
        this.context = context;
        this.arrayThongBaoDetail = arrayThongBaoDetail;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_thong_bao,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onClick(position);
            }
        });

        holder.tvContent.setText(arrayThongBaoDetail.get(position).getContent().toString());
        if(arrayThongBaoDetail.get(position).getIsRead()==1){
            holder.tvContent.setTypeface(Typeface.DEFAULT);
        }else holder.tvContent.setTypeface(Typeface.DEFAULT_BOLD);

//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");
        try {
            SimpleDateFormat sdfOld = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdfNew = new SimpleDateFormat("dd/M/yyyy hh:mm:ss");

            Date past = sdfOld.parse(arrayThongBaoDetail.get(position).getCreatedAt());
            Date now = Calendar.getInstance().getTime();

            Date past2 = sdfNew.parse(sdfNew.format(past));
            Date now2 = sdfNew.parse(sdfNew.format(now));

//            setTime(position,holder);
//            printDifference(past2,now2,holder);
            holder.tvTime.setText(Helper.getTime(past2,now2));
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return arrayThongBaoDetail.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvContent, tvTime;
        public ViewHolder(View itemView) {
            super(itemView);
            tvContent = itemView.findViewById(R.id.tvContent);
            tvTime = itemView.findViewById(R.id.tvTime);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onClick(int position);
    }


    public void printDifference(Date startDate, Date endDate, ViewHolder viewHolder) {
        viewHolder.tvTime.setText("");
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();


        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        Log.d("thongbaoadapter",elapsedDays+":"+elapsedHours+":"+elapsedMinutes+":"+elapsedSeconds) ;

        if(elapsedDays>0){
            viewHolder.tvTime.setText(elapsedDays+" ngày ");
        }
        if(elapsedHours>0){
            viewHolder.tvTime.setText(""+viewHolder.tvTime.getText() + elapsedHours + " giờ ");
        }
        if(elapsedMinutes>0){
            viewHolder.tvTime.setText(""+viewHolder.tvTime.getText() + elapsedMinutes + " phút ");
        }
        if(elapsedSeconds>0){
            viewHolder.tvTime.setText(""+viewHolder.tvTime.getText() + elapsedSeconds + " giây ");
        }
    }
}
