package com.company.meboo.meboo.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefUtil {

    public static void saveToken(Context context, String token){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Key.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Key.APP_TOKEN, token);
        editor.apply();
    }

    public static String getToken(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Key.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(Key.APP_TOKEN,null);
    }

    public static void saveString(Context context, String s, String key){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Key.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, s);
        editor.apply();
    }

    public static String getString(Context context, String key,String defValue){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Key.SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key,defValue);
    }
}
