package com.company.meboo.meboo.presenter.mLoginPresenter;

import android.util.Log;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.AccountModel;
import com.company.meboo.meboo.model.model.object.Account;
import com.company.meboo.meboo.model.model.object.AccountLogin;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.ui.activity.mLoginActivity.LoginView;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginPresenterImp extends BasePresenterImpl<LoginView> implements LoginPresenter {
    String token;
    public LoginPresenterImp(LoginView view) {
        super(view);
    }


    @Override
    public void onImvDeleteClick(int isAccount) {
        getView().onImvDeleteClick(isAccount);
    }

    @Override
    public void login(String name, String password) {
        token = FirebaseInstanceId.getInstance().getToken();
        Log.d("Firebase","Token: " + token);
        ApiUtils.getQlclService().login(name,password,token).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AccountLogin>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        getView().onLogInFail("Đăng nhập không thành công");
                    }

                    @Override
                    public void onNext(AccountLogin accountLogin) {
                        if(accountLogin.getStatus() == 1){
                            getView().onLogInSuccess(accountLogin.getData().getToken());
                        }else{
                            getView().onLogInFail("Đăng nhập onnext lỗi");
                        }

                    }
                });
    }


}
