package com.company.meboo.meboo.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.util.ApplicationContextSingleton;
import com.company.meboo.meboo.util.Key;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    String TAG = "TokenMyFirebase";
    private static final String ADMIN_CHANNEL_ID = "admin_channel";
    private static final int NOTIFICATION_ID = 100;

    @Override
    public void onNewToken(String s) {
        Log.d("Token", "Refreshed token: " + s);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Intent i = new Intent(getApplication(), NotificationClickService.class);

        i.putExtra(Key.ID, Integer.parseInt(remoteMessage.getData().get("id")));
        i.putExtra(Key.TYPE, Integer.parseInt(remoteMessage.getData().get("type")));

        Log.d("mfirebase", "id: " + remoteMessage.getData().get("id"));
        Log.d("mfirebase", "type: " + remoteMessage.getData().get("type"));

        PendingIntent pendingIntent = PendingIntent.getService(getApplicationContext(), Key.NOTIFY, i, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, ADMIN_CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }


    @Override
    public void onMessageSent(String s) {
        Log.d("Message", "Success");
        super.onMessageSent(s);
    }


}
