package com.company.meboo.meboo.base;

public interface BaseView<BPresenter extends BasePresenter> {
    BPresenter getPresenter();

    BPresenter createPresenter();

}
