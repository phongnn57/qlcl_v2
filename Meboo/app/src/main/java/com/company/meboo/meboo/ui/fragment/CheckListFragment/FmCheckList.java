package com.company.meboo.meboo.ui.fragment.CheckListFragment;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseFragment;
import com.company.meboo.meboo.model.model.object.CheckList;
import com.company.meboo.meboo.presenter.mCheckListPresenter.CheckListPresenter;
import com.company.meboo.meboo.presenter.mCheckListPresenter.CheckListPresenterImp;
import com.company.meboo.meboo.ui.activity.mBaoCaoXuLy.BaoCaoXuLyActivity;
import com.company.meboo.meboo.ui.adapter.CheckListDetailAdapter;
import com.company.meboo.meboo.util.EndlessRecyclerViewScrollListener;
import com.company.meboo.meboo.util.Key;

import java.util.ArrayList;
import java.util.logging.LogRecord;

public class FmCheckList extends BaseFragment<CheckListPresenter> implements CheckListView {
    CheckListDetailAdapter checkListDetailAdapter;
    ProgressBar progressBar;
    ArrayList<CheckList> listChecklist = new ArrayList<>();

    @Override
    public int getContentViewId() {
        return R.layout.fm_check_list;
    }

    @Override
    public void initializeComponents(View view) {
        getPresenter().initCheckList();
        listChecklist = getPresenter().getCheckList();
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        RecyclerView rvCheckList = view.findViewById(R.id.rvCheckList);
        checkListDetailAdapter = new CheckListDetailAdapter(getActivity(), listChecklist);

        checkListDetailAdapter.setOnClickListener(new CheckListDetailAdapter.OnClickListener() {
            @Override
            public void onGoodClick(int position) {

            }

            @Override
            public void onBadClick(int position) {
                Intent intent = new Intent(getActivity(), BaoCaoXuLyActivity.class);
                intent.putExtra(Key.BAD_CLICK, listChecklist.get(position));
                startActivity(intent);
            }
        });

        rvCheckList.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvCheckList.setLayoutManager(linearLayoutManager);
        rvCheckList.setAdapter(checkListDetailAdapter);
        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                progressBar.setVisibility(View.VISIBLE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getPresenter().loadNextData();
                        Log.d("ABC", "me trong load more");
                    }
                }, 500);
            }
        };
        rvCheckList.addOnScrollListener(scrollListener);


    }

    @Override
    public CheckListPresenter createPresenter() {
        return new CheckListPresenterImp(this);
    }

    @Override
    public void notifyData() {
        checkListDetailAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public Context gContext() {
        return getActivity();
    }
}
