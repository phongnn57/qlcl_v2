package com.company.meboo.meboo.ui.activity.mThemKhuVucActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.presenter.mThemKhuVucPresenter.ThemKhuVucPresenter;
import com.company.meboo.meboo.presenter.mThemKhuVucPresenter.ThemKhuVucPresenterImp;
import com.company.meboo.meboo.ui.adapter.RegionAdapter;
import com.company.meboo.meboo.util.Key;

import java.util.ArrayList;

public class ThemKhuVucActivity extends BaseActivity<ThemKhuVucPresenter> implements ThemKhuVucView, View.OnClickListener {
    RecyclerView rvRegion, rvSubRegion;
    RegionAdapter regionAdapter, choseRegionAdapter;
    ArrayList<SubRegion> listRegionsChose = new ArrayList<>();
    ArrayList<SubRegion> listRegionsApi = new ArrayList<>();
    TextView tvDelete;
    ImageView imvRegionAvatar;
    String filePath = "";
    EditText edtRegionName, edtRegionDesc;
    Button btnConfirm;
    int parentID = -1;

    @Override
    public int getContentViewId() {
        return R.layout.activity_them_khu_vuc;
    }

    @Override
    public void initializeComponents() {
        init();
        listRegionsApi = getPresenter().getListRegion();
        regionAdapter = new RegionAdapter(this, listRegionsApi, R.layout.region_item_tkv);
        choseRegionAdapter = new RegionAdapter(this, listRegionsChose, R.layout.region_item_tkv);
        rvRegion.setAdapter(regionAdapter);
        rvSubRegion.setAdapter(choseRegionAdapter);
        regionAdapter.setOnItemClickListener(new RegionAdapter.OnItemClickListener() {
            @Override
            public void onClickDelete(int position) {

            }

            @Override
            public void onClick(int position) {
                listRegionsChose.add(listRegionsApi.get(position));
                parentID = position;
                listRegionsApi.clear();
                regionAdapter.notifyDataSetChanged();
                choseRegionAdapter.notifyDataSetChanged();
            }
        });
    }

    public void init() {
        setSupportActionBar((Toolbar) findViewById(R.id.myToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        rvRegion = findViewById(R.id.rvRegion);
        rvSubRegion = findViewById(R.id.rvSubRegion);
        btnConfirm = findViewById(R.id.btnConfirm);
        imvRegionAvatar = findViewById(R.id.imvRegionAvatar);
        edtRegionDesc = findViewById(R.id.edtRegionDesc);
        edtRegionName = findViewById(R.id.edtRegionName);
        tvDelete = findViewById(R.id.tvDelete);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvRegion.setLayoutManager(linearLayoutManager);
        rvSubRegion.setLayoutManager(linearLayoutManager2);
        tvDelete.setOnClickListener(this);
        imvRegionAvatar.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
        getPresenter().callApiListRegion();
    }

    @Override
    public ThemKhuVucPresenter createPresenter() {
        return new ThemKhuVucPresenterImp(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void notifyRegionData() {
        regionAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishActivity() {
        Toast.makeText(this, "Thêm thành công", Toast.LENGTH_SHORT).show();
        this.finish();
    }

    @Override
    public Context gContext() {
        return this;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvDelete:
                if (listRegionsChose.size() > 0) {
                    listRegionsChose.clear();
                    choseRegionAdapter.notifyDataSetChanged();
                    getPresenter().callApiListRegion();
                    listRegionsApi = getPresenter().getListRegion();
                    regionAdapter.notifyDataSetChanged();
                    parentID = -1;
                }
                break;
            case R.id.imvRegionAvatar:
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, Key.PICK_IMAGE);
                break;
            case R.id.btnConfirm:
                String desc = edtRegionDesc.getText().toString();
                String name = edtRegionName.getText().toString();
                if (!TextUtils.isEmpty(name)) {
                    getPresenter().createRegion(parentID, name, desc, filePath);
                } else {
                    Toast.makeText(this, "Vui lòng nhập đủ thông tin", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Key.PICK_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            imvRegionAvatar.setImageURI(selectedImage);
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filePath = cursor.getString(columnIndex);
            cursor.close();
        }

    }
}

