package com.company.meboo.meboo.network;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataResponse<T> {
    @SerializedName("sub")
    @Expose
    private ArrayList<T> sub = null;
    @SerializedName("breadcrumb")
    @Expose
    private List<Object> breadcrumb = null;

    public ArrayList<T> getSub() {
        return sub;
    }

    public void setSub(ArrayList<T> sub) {
        this.sub = sub;
    }

    public List<Object> getBreadcrumb() {
        return breadcrumb;
    }

    public void setBreadcrumb(List<Object> breadcrumb) {
        this.breadcrumb = breadcrumb;
    }

}