package com.company.meboo.meboo.presenter.mQuanLyKhuVucPresenter;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.SubRegion;

import java.util.ArrayList;

public interface QuanLyKhuVucPresenter extends BasePresenter {
    ArrayList<SubRegion> getListRegion();
}
