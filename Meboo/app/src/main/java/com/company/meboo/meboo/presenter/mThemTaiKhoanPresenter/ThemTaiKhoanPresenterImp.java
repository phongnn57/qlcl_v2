package com.company.meboo.meboo.presenter.mThemTaiKhoanPresenter;

import android.util.Log;
import android.widget.Toast;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.SubQuarter;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.network.DataResponse;
import com.company.meboo.meboo.network.Response;
import com.company.meboo.meboo.ui.activity.ThemTaiKhoanActivity.ThemTaiKhoanView;
import com.company.meboo.meboo.util.PrefUtil;
import com.google.gson.JsonObject;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ThemTaiKhoanPresenterImp extends BasePresenterImpl<ThemTaiKhoanView> implements ThemTaiKhoanPresenter {
    ArrayList<SubRegion> listRegions = new ArrayList<>();
    ArrayList<SubQuarter> listQuarter = new ArrayList<>();

    public ThemTaiKhoanPresenterImp(ThemTaiKhoanView view) {
        super(view);
        callApiListRegion();
        callApiListQuarter();
    }

    public void callApiListRegion() {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().getRegionList(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<DataResponse<SubRegion>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<DataResponse<SubRegion>> dataResponseResponse) {
                        listRegions.clear();
                        listRegions.addAll(dataResponseResponse.getData().getSub());
                        getView().notifyRegionData();
                    }
                });
    }

    public void callApiListQuarter() {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().getQuarterList(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<DataResponse<SubQuarter>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<DataResponse<SubQuarter>> dataResponseResponse) {
                        listQuarter.clear();
                        listQuarter.addAll(dataResponseResponse.getData().getSub());
                        getView().notifyQuarterData();
                    }
                });
    }

    @Override
    public ArrayList<SubRegion> getListRegion() {
        return listRegions;
    }

    @Override
    public ArrayList<SubQuarter> getListQuarter() {
        return listQuarter;
    }

    @Override
    public void createAccount(String userName, String password, int type, int quarterId, ArrayList<SubRegion> listRegions, String name, String phone, String email, String birthday) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().sendAccount(token, userName, password, type, quarterId, listRegions, name, phone, email, birthday)
                .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("ABC","loi post");
                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        getView().finishActivity();
                    }
                });
    }

}
