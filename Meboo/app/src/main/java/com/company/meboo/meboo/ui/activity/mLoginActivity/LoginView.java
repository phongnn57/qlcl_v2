package com.company.meboo.meboo.ui.activity.mLoginActivity;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mLoginPresenter.LoginPresenter;

public interface LoginView extends BaseView<LoginPresenter> {
    void onLogInSuccess(String token);

    void onLogInFail(String s);

    void onImvDeleteClick(int isAccount);
}
