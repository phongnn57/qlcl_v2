package com.company.meboo.meboo.model.model.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DataReportDetail {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("images")
    @Expose
    private ArrayList<String> images;
    @SerializedName("desc")
    @Expose
    private Object desc;
    @SerializedName("checklist_id")
    @Expose
    private Integer checklistId;
    @SerializedName("recommend")
    @Expose
    private Object recommend;
    @SerializedName("user_id")
    @Expose
    private Object userId;
    @SerializedName("deadline")
    @Expose
    private Object deadline;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("is_fixed")
    @Expose
    private Integer isFixed;
    @SerializedName("images_fixed")
    @Expose
    private Object imagesFixed;
    @SerializedName("result_fixed")
    @Expose
    private Object resultFixed;
    @SerializedName("checklist")
    @Expose
    private Checklist2 checklist;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public Object getDesc() {
        return desc;
    }

    public void setDesc(Object desc) {
        this.desc = desc;
    }

    public Integer getChecklistId() {
        return checklistId;
    }

    public void setChecklistId(Integer checklistId) {
        this.checklistId = checklistId;
    }

    public Object getRecommend() {
        return recommend;
    }

    public void setRecommend(Object recommend) {
        this.recommend = recommend;
    }

    public Object getUserId() {
        return userId;
    }

    public void setUserId(Object userId) {
        this.userId = userId;
    }

    public Object getDeadline() {
        return deadline;
    }

    public void setDeadline(Object deadline) {
        this.deadline = deadline;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Object getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getIsFixed() {
        return isFixed;
    }

    public void setIsFixed(Integer isFixed) {
        this.isFixed = isFixed;
    }

    public Object getImagesFixed() {
        return imagesFixed;
    }

    public void setImagesFixed(Object imagesFixed) {
        this.imagesFixed = imagesFixed;
    }

    public Object getResultFixed() {
        return resultFixed;
    }

    public void setResultFixed(Object resultFixed) {
        this.resultFixed = resultFixed;
    }

    public Checklist2 getChecklist() {
        return checklist;
    }

    public void setChecklist(Checklist2 checklist) {
        this.checklist = checklist;
    }

}