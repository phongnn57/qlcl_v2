package com.company.meboo.meboo.util;

public class Key {
    public static final String CODE_KHU_VUC = "code_khu_vuc";
    public static final int CODE_REQUEST_KV_2 = 999;
    public static final String KHU_VUC = "khu_vuc";
    public static final int ADMIN = 1;
    public static final int KIEM_SOAT_CHAT_LUONG = 2;
    public static final int GIAM_DOC = 3;
    public static final int TRUONG_BO_PHAN = 4;
    public static final int NHAN_VIEN = 5;
    public static final int TONG_GIAM_DOC = 6;
    public static final int PICK_IMAGE = 998;
    public static final int NOTIFY = 996;
    public static final String BAD_CLICK = "bad_click";
    public static final int SELECT_MULTI_PICTURES = 997;
    public static final String SHARED_PREFERENCES = "app_shared_preferences";
    public static final String APP_TOKEN = "app_token";
    public static final String NOTIF = "NOTIF";

    public static final String ID = "ID";
    public static final String TYPE = "TYPE";

    public static final String NAME_CHECK_LIST = "namechecklist";
    public static final String QUARTER_CHECK_LIST = "quarterchecklist";
    public static final String DESC_CHECK_LIST = "descchecklist";
    public static final String START_TIME = "start_time";
    public static final String END_TIME = "end_time";
    public static final String RANGE_TIME = "range_time";
}
