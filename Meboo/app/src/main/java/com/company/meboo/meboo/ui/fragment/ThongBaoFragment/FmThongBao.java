package com.company.meboo.meboo.ui.fragment.ThongBaoFragment;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseFragment;
import com.company.meboo.meboo.model.model.object.ThongBaoDetail;
import com.company.meboo.meboo.presenter.mThongBaoPresenter.ThongBaoPresenter;
import com.company.meboo.meboo.presenter.mThongBaoPresenter.ThongBaoPresenterImp;
import com.company.meboo.meboo.ui.activity.mChiTietYeuCauGiaHan.ChiTietYeuCauGiaHanActivity;
import com.company.meboo.meboo.ui.activity.mThongBaoChiTiet.ThongBaoChiTietActivity;
import com.company.meboo.meboo.ui.adapter.ThongBaoAdapter;
import com.company.meboo.meboo.util.EndlessRecyclerViewScrollListener;
import com.company.meboo.meboo.util.Key;

import java.util.ArrayList;

public class FmThongBao extends BaseFragment<ThongBaoPresenter> implements ThongBaoView, View.OnClickListener {
    ImageView imvReadAll;
    RecyclerView rvThongBao;
    ThongBaoAdapter thongBaoAdapter;
    ArrayList<ThongBaoDetail> thongBaoDetails;
    ProgressBar progressBar;

    @Override
    public int getContentViewId() {
        return R.layout.fm_thong_bao;
    }

    @Override
    public void initializeComponents(View view) {
        getPresenter().callApiListNotification();

        progressBar = view.findViewById(R.id.progressBar);
        imvReadAll = view.findViewById(R.id.imvReadAll);
        rvThongBao = view.findViewById(R.id.rvThongBao);
        progressBar.setVisibility(View.GONE);

        imvReadAll.setOnClickListener(this);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvThongBao.setLayoutManager(linearLayoutManager);

        thongBaoDetails = getPresenter().getListNotification();
        thongBaoAdapter = new ThongBaoAdapter(getActivity(), thongBaoDetails);
        thongBaoAdapter.setOnItemClickListener(new ThongBaoAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {

                switch (thongBaoDetails.get(position).getPayload().getType()) {
                    case 1:
                        Toast.makeText(getActivity(), "id: " + thongBaoDetails.get(position).getPayload().getId(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), ThongBaoChiTietActivity.class);
                        intent.putExtra(Key.ID, thongBaoDetails.get(position).getPayload().getId());
                        startActivity(intent);
                        break;
                    case 2:
                        Toast.makeText(getActivity(), "id: " + thongBaoDetails.get(position).getPayload().getId(), Toast.LENGTH_SHORT).show();
                        Intent intent2 = new Intent(getActivity(), ChiTietYeuCauGiaHanActivity.class);
                        intent2.putExtra(Key.ID, thongBaoDetails.get(position).getPayload().getId());
                        startActivity(intent2);
                        break;

                }
                getPresenter().read(getPresenter().getListNotification().get(position).getId());
            }
        });

        rvThongBao.setAdapter(thongBaoAdapter);

        EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                progressBar.setVisibility(View.VISIBLE);
                android.os.Handler handler = new android.os.Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getPresenter().loadMore();

                    }
                }, 500);

            }
        };
        rvThongBao.addOnScrollListener(endlessRecyclerViewScrollListener);

    }

    @Override
    public ThongBaoPresenter createPresenter() {
        return new ThongBaoPresenterImp(this);
    }

    @Override
    public Context gContext() {
        return getContext();
    }

    @Override
    public void notifyData() {
        thongBaoAdapter.notifyDataSetChanged();
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        getPresenter().readAll();
    }
}
