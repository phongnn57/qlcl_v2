package com.company.meboo.meboo.presenter.mYeuCauGiaHanPresenter;

import android.util.Log;
import android.widget.Toast;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.DataReportDetail;
import com.company.meboo.meboo.model.model.object.ReportDetail;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.ui.activity.mYeuCauGiaHan.YeuCauGiaHanView;
import com.company.meboo.meboo.util.PrefUtil;
import com.google.gson.JsonObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class YeuCauGiaHanPresenterImp extends BasePresenterImpl<YeuCauGiaHanView> implements YeuCauGiaHanPresenter {
    DataReportDetail dataReportDetail = new DataReportDetail();

    public YeuCauGiaHanPresenterImp(YeuCauGiaHanView view) {
        super(view);
    }

    @Override
    public void getNotifyDetail(int i) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().getReportDetail(token, i).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ReportDetail>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ReportDetail reportDetail) {
                        if (reportDetail.getStatus() == 1) {
                            Log.d("mfirebase", "lay du lieu thanh cong");
                            dataReportDetail = reportDetail.getData();
                            getView().setData(dataReportDetail);
                        }
                    }
                });
    }

    @Override
    public void sendNewReport(String id, String newDeadLine, String desc) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        RequestBody rbId = RequestBody.create(MediaType.parse("text/plain"), id);
        RequestBody rbNewDeadLine = RequestBody.create(MediaType.parse("text/plain"), newDeadLine);
        RequestBody rbDesc = RequestBody.create(MediaType.parse("text/plain"), desc);
        ApiUtils.getQlclService().createReportExtend(token, rbId, rbNewDeadLine, rbDesc).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        Log.d("YCGH","me on onnext");
                        if (jsonObject.get("status").getAsInt() == 1) {
                            Log.d("YCGH","me on onnext == 1");
                            Toast.makeText(getView().gContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getView().gContext(), jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

}
