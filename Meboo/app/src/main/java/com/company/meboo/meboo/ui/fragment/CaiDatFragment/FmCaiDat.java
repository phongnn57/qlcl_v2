package com.company.meboo.meboo.ui.fragment.CaiDatFragment;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseFragment;
import com.company.meboo.meboo.model.model.object.CaiDat;
import com.company.meboo.meboo.presenter.mCaiDatPresenter.CaiDatPresenter;
import com.company.meboo.meboo.presenter.mCaiDatPresenter.CaiDatPresenterImp;
import com.company.meboo.meboo.ui.activity.mQuanLyKhuVuc.QuanLyKhuVucActivity;
import com.company.meboo.meboo.ui.activity.mQuanLyTaiKhoanActivity.QuanLyTaiKhoanActivity;
import com.company.meboo.meboo.ui.adapter.JsonCaiDatAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class FmCaiDat extends BaseFragment<CaiDatPresenter> implements CaiDatView, View.OnClickListener {
//    RecyclerView rcvCaiDat;
    TextView tvQuanLyTaiKhoan, tvQuanLyKhuVuc;
    @Override
    public int getContentViewId() {
        return R.layout.fm_cai_dat;
    }

    @Override
    public void initializeComponents(View view) {
//        rcvCaiDat = view.findViewById(R.id.rcvCaiDat);
//        init(view);
        tvQuanLyTaiKhoan = view.findViewById(R.id.tvQuanLyTaiKhoan);
        tvQuanLyKhuVuc = view.findViewById(R.id.tvQuanLyKhuVuc);
        tvQuanLyTaiKhoan.setOnClickListener(this);
        tvQuanLyKhuVuc.setOnClickListener(this);
    }

    @Override
    public CaiDatPresenter createPresenter() {
        return new CaiDatPresenterImp(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvQuanLyTaiKhoan:
                Intent intent = new Intent(getActivity(),QuanLyTaiKhoanActivity.class);
                startActivity(intent);
                break;
            case R.id.tvQuanLyKhuVuc:
                Intent intent2 = new Intent(getActivity(),QuanLyKhuVucActivity.class);
                startActivity(intent2);
                break;
        }
    }

//    public String readJSon(){
//        BufferedReader reader = null;
//        StringBuilder stringBuilder = null;
//        try {
//            stringBuilder = new StringBuilder();
//            reader = new BufferedReader(
//                    new InputStreamReader(getActivity().getAssets().open("caidat.txt"), "UTF-8"));
//
//            // do reading, usually loop until end of file reading
//            String mLine;
//            while ((mLine = reader.readLine()) != null) {
//                stringBuilder.append(mLine);
//            }
//        } catch (IOException e) {
//            //log the exception
//        } finally {
//            if (reader != null) {
//                try {
//                    reader.close();
//                } catch (IOException e) {
//                    //log the exception
//                }
//            }
//        }
//        return stringBuilder.toString();
//    }

//    public void init(View view){
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(),LinearLayoutManager.VERTICAL,false);
//        rcvCaiDat.setLayoutManager(linearLayoutManager);
//
//
//        ArrayList<CaiDat> listCD = new ArrayList<>();
//
//        try {
//            String contentJSon = readJSon();
//            JSONObject o1 = new JSONObject(contentJSon);
//            JSONArray listCaidat = o1.getJSONArray("caidat");
//            for(int i=0; i<listCaidat.length();i++){
//                JSONObject jsonObject = listCaidat.getJSONObject(i);
//                String name = jsonObject.getString("name");
//                String urlImage = jsonObject.getString("image");
//                int id = getActivity().getResources().getIdentifier(urlImage,"drawable",getActivity().getPackageName());
//                listCD.add(new CaiDat(id, name));
//            }
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        if(listCD.size()>0){
//            JsonCaiDatAdapter jsonCaiDatAdapter = new JsonCaiDatAdapter(getActivity(),listCD);
//            rcvCaiDat.setAdapter(jsonCaiDatAdapter);
//            jsonCaiDatAdapter.notifyDataSetChanged();
//        }
//    }

}
