package com.company.meboo.meboo.presenter.mThongBaoChiTietPresenter;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.Checklist2;
import com.company.meboo.meboo.model.model.object.DataReportDetail;

import java.util.ArrayList;

public interface ThongBaoChiTietPresenter extends BasePresenter {
    void getNotifyDetail(int i);
    DataReportDetail getDataReportDetail();
    Checklist2 getChecklist();
}
