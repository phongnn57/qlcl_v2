package com.company.meboo.meboo.ui.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.model.model.object.KhuVuc;

import java.util.ArrayList;

public class ChonKhuVucAdapter extends RecyclerView.Adapter<ChonKhuVucAdapter.ViewHolder> implements Filterable{
    Context context;
    ArrayList<KhuVuc> listKhuVuc;
    ArrayList<KhuVuc> listKhuVucFiltered;
    OnItemClickListener onItemClickListener;

    public ChonKhuVucAdapter(Context context, ArrayList<KhuVuc> listKhuVuc) {
        this.context = context;
        this.listKhuVuc = listKhuVuc;
        listKhuVucFiltered = listKhuVuc;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.chon_khu_vuc_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.tvKhuVuc.setText(listKhuVucFiltered.get(position).getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listKhuVucFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    listKhuVucFiltered = listKhuVuc;
                } else {
                    ArrayList<KhuVuc> filteredList = new ArrayList<>();
                    for (KhuVuc kv : listKhuVuc) {
                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        if (kv.getName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(kv);
                        }
                    }

                    listKhuVucFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = listKhuVucFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                listKhuVucFiltered = (ArrayList<KhuVuc>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvKhuVuc;

        public ViewHolder(View itemView) {
            super(itemView);
            tvKhuVuc = itemView.findViewById(R.id.tvKhuVuc);
        }
    }



    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }

}
