package com.company.meboo.meboo.firebase;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.company.meboo.meboo.ui.activity.mChiTietYeuCauGiaHan.ChiTietYeuCauGiaHanActivity;
import com.company.meboo.meboo.ui.activity.mLoginActivity.LoginActivity;
import com.company.meboo.meboo.ui.activity.mThongBaoChiTiet.ThongBaoChiTietActivity;
import com.company.meboo.meboo.util.Key;

public class NotificationClickService extends Service {
    private static final String TAG = "NotificationClick";

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("mfirebase", "me on service");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        int i = (int) intent.getExtras().get(Key.ID);
        switch ((int) intent.getExtras().get(Key.TYPE)) {
            case 1:
                Intent intent2 = new Intent(getApplicationContext(), ThongBaoChiTietActivity.class);
                intent2.putExtra(Key.ID, i);
                PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 9, intent2, PendingIntent.FLAG_ONE_SHOT);
                try {
                    pendingIntent.send();
                } catch (PendingIntent.CanceledException e) {
                    e.printStackTrace();
                }
                break;
            case 2:
                Intent intent3 = new Intent(getApplicationContext(),ChiTietYeuCauGiaHanActivity.class);
                intent3.putExtra(Key.ID,i);
                PendingIntent pendingIntent3 = PendingIntent.getActivity(getApplicationContext(),9,intent3,PendingIntent.FLAG_ONE_SHOT);
                try {
                    pendingIntent3.send();
                } catch (PendingIntent.CanceledException e) {
                    e.printStackTrace();
                }
                break;
        }

        return START_STICKY;
    }
}
