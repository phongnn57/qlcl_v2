package com.company.meboo.meboo.ui.fragment.BaoCaoFragment;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mBaoCaoPresenter.BaoCaoPresenter;

public interface BaoCaoView extends BaseView<BaoCaoPresenter>{
    Context gContext();

    void notifyData();
}
