package com.company.meboo.meboo.ui.activity.mMainActivity;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mMainPresenter.MainPresenter;

public interface MainView extends BaseView<MainPresenter>{
}
