package com.company.meboo.meboo.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.model.model.object.Account;
import com.company.meboo.meboo.util.Key;

import java.util.ArrayList;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.ViewHolder>{
    Context context;
    ArrayList<Account> list;
    OnItemClickListener onItemClickListener;

    public AccountAdapter(Context context, ArrayList<Account> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.account_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.imvDeleteAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickListener.onClick(position);
            }
        });

        holder.tvName.setText(list.get(position).getName());
        switch (list.get(position).getType()){
            case Key.ADMIN:
                holder.tvType.setText("Admin");
                break;
            case Key.GIAM_DOC:
                holder.tvType.setText("Giám đốc");
                break;
            case Key.TRUONG_BO_PHAN:
                holder.tvType.setText("Trưởng bộ phận");
                break;
            case Key.KIEM_SOAT_CHAT_LUONG:
                holder.tvType.setText("Kiểm soát chất lượng");
                break;
            case Key.NHAN_VIEN:
                holder.tvType.setText("Nhân viên");
                break;
            case Key.TONG_GIAM_DOC:
                holder.tvType.setText("Tổng giám đốc");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvName, tvType;
        ImageView imvDeleteAccount;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tvName);
            tvType = itemView.findViewById(R.id.tvType);
            imvDeleteAccount = itemView.findViewById(R.id.imvDeleteAccount);
        }
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public interface OnItemClickListener{
        void onClick(int position);
    }

    public void remove(int position){
        list.remove(position);
    }
}
