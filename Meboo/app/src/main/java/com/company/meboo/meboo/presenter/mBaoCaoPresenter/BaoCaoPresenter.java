package com.company.meboo.meboo.presenter.mBaoCaoPresenter;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.QuarterReport;
import com.company.meboo.meboo.model.model.object.SubRegion;

import java.util.ArrayList;

public interface BaoCaoPresenter extends BasePresenter{
    void getData(String startTime, String endTime);
    ArrayList<QuarterReport> getQuarterReport();
}
