package com.company.meboo.meboo.ui.activity.mBaoCaoHoanThanh;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.presenter.mBaoCaoHoanThanhPresenter.BaoCaoHoanThanhPresenter;
import com.company.meboo.meboo.presenter.mBaoCaoHoanThanhPresenter.BaoCaoHoanThanhPresenterImp;
import com.company.meboo.meboo.ui.adapter.ImageAdapter;
import com.company.meboo.meboo.util.Key;

import java.util.ArrayList;

public class BaoCaoHoanThanhActivity extends BaseActivity<BaoCaoHoanThanhPresenter> implements BaoCaoHoanThanhView, View.OnClickListener {
    TextView tvNameChecklist, tvQuarterChecklist, tvDescChecklist, tvAddImage;
    EditText edtDesc;
    RecyclerView rvImage;
    Button btnConfirm;
    ImageAdapter imageAdapter;
    ArrayList<String> filePaths = new ArrayList<>();
    ArrayList<String> listImageUri = new ArrayList<>();
    int id;
    @Override
    public int getContentViewId() {
        return R.layout.activity_bao_cao_hoan_thanh;
    }

    @Override
    public void initializeComponents() {
        setSupportActionBar((Toolbar) findViewById(R.id.myToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tvNameChecklist = findViewById(R.id.tvNameChecklist);
        tvQuarterChecklist = findViewById(R.id.tvQuarterChecklist);
        tvDescChecklist = findViewById(R.id.tvDescChecklist);
        tvAddImage = findViewById(R.id.tvAddImage);
        edtDesc = findViewById(R.id.edtDesc);
        btnConfirm = findViewById(R.id.btnConfirm);

        tvAddImage.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);

        rvImage = findViewById(R.id.rvImage);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        rvImage.setLayoutManager(linearLayoutManager);

        id= getIntent().getIntExtra(Key.ID, 0);

        tvNameChecklist.setText(getIntent().getStringExtra(Key.NAME_CHECK_LIST));
        tvQuarterChecklist.setText(getIntent().getStringExtra(Key.QUARTER_CHECK_LIST));
        tvDescChecklist.setText(getIntent().getStringExtra(Key.DESC_CHECK_LIST));
    }

    @Override
    public BaoCaoHoanThanhPresenter createPresenter() {
        return new BaoCaoHoanThanhPresenterImp(this);
    }


    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tvAddImage:
                Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                //**These following line is the important one!
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), Key.SELECT_MULTI_PICTURES);
                break;
            case R.id.btnConfirm:
                if(!TextUtils.isEmpty(edtDesc.getText().toString())){
                    getPresenter().sendCompleteReport(edtDesc.getText().toString(), filePaths, id);
                    finish();
                }else Toast.makeText(this, "Vui lòng nhập mô tả", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Key.SELECT_MULTI_PICTURES) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getClipData() != null) {
                    int count = data.getClipData().getItemCount(); //evaluate the count before the for loop --- otherwise, the count is evaluated every loop.
                    for (int i = 0; i < count; i++) {
                        Uri imageUri = data.getClipData().getItemAt(i).getUri();
                        listImageUri.add(imageUri.toString());
                        //do something with the image (save it to some directory or whatever you need to do with it here)

                    }
                    //get image path
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    for(int j=0; j<listImageUri.size();j++){
                        Cursor cursor = getContentResolver().query(Uri.parse(listImageUri.get(j)),
                                filePathColumn, null, null, null);
                        cursor.moveToFirst();

                        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                        filePaths.add(cursor.getString(columnIndex));
                        cursor.close();
                    }

                    //adapter
                    imageAdapter = new ImageAdapter(this, listImageUri);
                    imageAdapter.setOnClickListener(new ImageAdapter.OnClickListener() {
                        @Override
                        public void onClickDelete(int position) {
                            listImageUri.remove(position);
                            imageAdapter.notifyDataSetChanged();
                        }
                    });
                    rvImage.setAdapter(imageAdapter);
                } else if (data.getData() != null) {
                    String imagePath = data.getData().getPath();
                    //do something with the image (save it to some directory or whatever you need to do with it here)
                }
            }
        }
    }

    @Override
    public Context gContext() {
        return this;
    }
}
