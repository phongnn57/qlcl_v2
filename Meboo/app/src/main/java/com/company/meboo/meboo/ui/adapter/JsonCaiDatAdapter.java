package com.company.meboo.meboo.ui.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.model.model.object.CaiDat;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class JsonCaiDatAdapter extends RecyclerView.Adapter<JsonCaiDatAdapter.ViewHolder> {
    Context context;
    ArrayList<CaiDat> listCaiDat;

    public JsonCaiDatAdapter(Context context, ArrayList<CaiDat> listCaiDat) {
        this.context = context;
        this.listCaiDat = listCaiDat;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_json_cai_dat,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.tvName.setText(listCaiDat.get(position).getName());

        holder.imvImage.setImageResource(listCaiDat.get(position).getUrlImage());
    }

    @Override
    public int getItemCount() {
        return listCaiDat.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvName;
        CircleImageView imvImage;

        public ViewHolder(View itemView) {
            super(itemView);
            tvName= itemView.findViewById(R.id.tvName);
            imvImage= itemView.findViewById(R.id.imvImage);
        }
    }
}
