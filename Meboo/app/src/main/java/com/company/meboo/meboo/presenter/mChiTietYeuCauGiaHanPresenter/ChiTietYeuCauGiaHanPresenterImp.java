package com.company.meboo.meboo.presenter.mChiTietYeuCauGiaHanPresenter;

import android.util.Log;
import android.widget.Toast;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.DataReport;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.network.Response;
import com.company.meboo.meboo.ui.activity.mChiTietYeuCauGiaHan.ChiTietYeuCauGiaHanView;
import com.company.meboo.meboo.util.PrefUtil;
import com.google.android.gms.common.api.Api;
import com.google.gson.JsonObject;

import java.util.Date;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChiTietYeuCauGiaHanPresenterImp extends BasePresenterImpl<ChiTietYeuCauGiaHanView> implements ChiTietYeuCauGiaHanPresenter {
    public ChiTietYeuCauGiaHanPresenterImp(ChiTietYeuCauGiaHanView view) {
        super(view);
    }

    @Override
    public void callApiReportExtended(int id) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().getDetailReportExtend(token, id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<Response<DataReport>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Response<DataReport> dataReportResponse) {
                getView().setData(dataReportResponse.getData());
            }
        });
    }


    @Override
    public void reject(int id, String reasonReject) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().rejectExtend(token, id, 1, reasonReject).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<JsonObject>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(JsonObject jsonObject) {
                if (jsonObject.get("status").getAsInt() == 1) {
                    Toast.makeText(getView().gContext(), "Từ chối thành công", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getView().gContext(), jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void accept(int id, String date) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().acceptExtend(token, id, 1, date).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(new Subscriber<JsonObject>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(JsonObject jsonObject) {
                if (jsonObject.get("status").getAsInt() == 1) {
                    Toast.makeText(getView().gContext(), "Chấp nhận thành công", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getView().gContext(), jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
