package com.company.meboo.meboo.ui.activity.ThemTaiKhoanActivity;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mThemTaiKhoanPresenter.ThemTaiKhoanPresenter;

public interface ThemTaiKhoanView extends BaseView<ThemTaiKhoanPresenter>{
    void notifyRegionData();

    void notifyQuarterData();


    void finishActivity();

    Context gContext();
}
