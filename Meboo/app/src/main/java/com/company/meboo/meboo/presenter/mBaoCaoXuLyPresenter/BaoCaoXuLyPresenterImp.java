package com.company.meboo.meboo.presenter.mBaoCaoXuLyPresenter;

import android.preference.MultiSelectListPreference;
import android.widget.Toast;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.network.DataResponse;
import com.company.meboo.meboo.network.Response;
import com.company.meboo.meboo.ui.activity.mBaoCaoXuLy.BaoCaoXuLyView;
import com.company.meboo.meboo.util.PrefUtil;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BaoCaoXuLyPresenterImp extends BasePresenterImpl<BaoCaoXuLyView> implements BaoCaoXuLyPresenter {
    ArrayList<SubRegion> listRegions = new ArrayList<>();
    public BaoCaoXuLyPresenterImp(BaoCaoXuLyView view) {
        super(view);
    }

    public void callApiListRegion() {
        String token = "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6Ly9xbGNsLmZpdG1lLnZuL2FwaS92MS9sb2dpbiIsImlhdCI6MTUzMTkwMTgxMSwiZXhwIjoxNTMxOTA1NDExLCJuYmYiOjE1MzE5MDE4MTEsImp0aSI6ImhSd0FDOVBsU2xYT1ExRFMifQ.pH4JoaYGWnxLiEQH7VQwrZGr4GY6wulL-04D4NXdUvE";
        ApiUtils.getQlclService().getRegionList(token).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<DataResponse<SubRegion>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<DataResponse<SubRegion>> dataResponseResponse) {
                        listRegions.clear();
                        listRegions.addAll(dataResponseResponse.getData().getSub());
                        getView().notifyRegionData();
                    }
                });
    }

    @Override
    public void createReport(ArrayList<String> filePaths, String desc, String recommend, String deadline, String id, ArrayList<String> idQuarters) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());

        RequestBody rbDesc = RequestBody.create(MediaType.parse("text/plain"),desc);
        RequestBody rbRecommend = RequestBody.create(MediaType.parse("text/plain"),recommend);
        RequestBody rbDeadLine = RequestBody.create(MediaType.parse("text/plain"),deadline);
        RequestBody rbId = RequestBody.create(MediaType.parse("text/plain"),id);

        ArrayList<MultipartBody.Part> listImage = new ArrayList<>();
        for(String filePath : filePaths){
            File file = new File(filePath);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"),file);
            MultipartBody.Part image = MultipartBody.Part.createFormData("images[]", file.getName(), reqFile);
            listImage.add(image);
        }

        ApiUtils.getQlclService().createReport(token,listImage,rbDesc,rbRecommend,rbDeadLine,rbId,idQuarters)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        Toast.makeText(getView().gContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
                    }
                });
    }


    @Override
    public ArrayList<SubRegion> getListRegion() {
        return listRegions;
    }
}
