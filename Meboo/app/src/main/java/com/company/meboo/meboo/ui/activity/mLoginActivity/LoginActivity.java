package com.company.meboo.meboo.ui.activity.mLoginActivity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.company.meboo.meboo.firebase.MyFirebaseMessagingService;
import com.company.meboo.meboo.ui.activity.mChonKhuVucActivity.ChonKhuVucActivity;
import com.company.meboo.meboo.util.Helper;
import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.presenter.mLoginPresenter.LoginPresenter;
import com.company.meboo.meboo.presenter.mLoginPresenter.LoginPresenterImp;
import com.company.meboo.meboo.util.PrefUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;

public class LoginActivity extends BaseActivity<LoginPresenter> implements LoginView, View.OnClickListener {
    EditText edtAccount, edtPassword;
    ImageView imvDelAcc, imvDelPass;
    Button btnLogin;
    public static boolean isAppRunning=true;

    @Override
    public int getContentViewId() {
        return R.layout.activity_login;
    }

    @Override
    public void initializeComponents() {
        Intent intent = new Intent(this, MyFirebaseMessagingService.class);
        startService(intent);
        findViewByIDs();
        ShowHideImvByEdt();
        Log.d("RUNNING","Login Activity");
    }

    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenterImp(this);
    }

    public void findViewByIDs() {
        edtAccount = findViewById(R.id.edtAccount);
        edtPassword = findViewById(R.id.edtPassword);
        imvDelAcc = findViewById(R.id.imvDeleteAccount);
        imvDelPass = findViewById(R.id.imvDeletePassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        imvDelAcc.setOnClickListener(this);
        imvDelPass.setOnClickListener(this);
        setupUI(findViewById(R.id.parent));
    }

    public void setupUI(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Helper.hideSoftKeyboard(LoginActivity.this);
                    return false;
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                String acc = edtAccount.getText().toString();
                String pass = edtPassword.getText().toString();
                if (!TextUtils.isEmpty(acc) && !TextUtils.isEmpty(pass)) {
                    getPresenter().login(acc, pass);
                }

                break;
            case R.id.imvDeleteAccount:
                FirebaseMessaging fm = FirebaseMessaging.getInstance();
                fm.send(new RemoteMessage.Builder("210220029331" + "@gcm.googleapis.com")
//                        .setMessageId(Integer.toString(msgId.incrementAndGet()))
                        .setMessageId("m-123")
                        .addData("my_message", "Hello World")
                        .addData("my_action","SAY_HELLO")
                        .build());

                getPresenter().onImvDeleteClick(1);
                break;
            case R.id.imvDeletePassword:
                getPresenter().onImvDeleteClick(2);
                break;
        }

    }

    @Override
    public void onLogInSuccess(String token) {
        Intent intent = new Intent(LoginActivity.this, ChonKhuVucActivity.class);
        PrefUtil.saveToken(this,token);
        Log.d("token",token);
        FirebaseMessaging.getInstance().subscribeToTopic("news")
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        String msg = "success";
                        if (!task.isSuccessful()) {
                            msg = "fail";
                        }
                        Log.d("notify", msg);
                        Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_SHORT).show();
                    }
                });
        startActivity(intent);
    }

    @Override
    public void onLogInFail(String s) {
        Helper.showMessage(s,this);
    }

    @Override
    public void onImvDeleteClick(int isAccount) {
        if (isAccount == 1) {
            edtAccount.setText("");
            imvDelAcc.setVisibility(View.GONE);
        } else {
            edtPassword.setText("");
            imvDelPass.setVisibility(View.GONE);
        }
    }


    public void ShowHideImvByEdt() {
        imvDelAcc.setVisibility(View.GONE);
        imvDelPass.setVisibility(View.GONE);
        edtAccount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    imvDelAcc.setVisibility(View.GONE);

                } else {
                    imvDelAcc.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 0) {
                    imvDelPass.setVisibility(View.GONE);
                }
                if (!imvDelPass.isShown()) imvDelPass.setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }
}
