package com.company.meboo.meboo.ui.activity.mChonKhuVucActivity;

import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.company.meboo.meboo.firebase.FCMApiUtils;
import com.company.meboo.meboo.firebase.FirebaseApiService;
import com.company.meboo.meboo.firebase.FirebaseIDService;
import com.company.meboo.meboo.firebase.Notify;
import com.company.meboo.meboo.firebase.NotifyDetail;
import com.company.meboo.meboo.model.model.object.KhuVuc;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.ui.activity.mChonKhuVucActivity2.ChonKhuVucActivity2;
import com.company.meboo.meboo.ui.activity.mLoginActivity.LoginActivity;
import com.company.meboo.meboo.ui.activity.mMainActivity.MainActivity;
import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.presenter.mChonKhuVucPresenter.ChonKhuVucPresenter;
import com.company.meboo.meboo.presenter.mChonKhuVucPresenter.ChonKhuVucPresenterImp;
import com.company.meboo.meboo.util.Key;
import com.google.android.gms.common.api.Api;
import com.google.firebase.iid.FirebaseInstanceId;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChonKhuVucActivity extends BaseActivity<ChonKhuVucPresenter> implements ChonKhuVucView, View.OnClickListener {
    Button btnContinue;
    TextView tvKhuVuc;

    @Override
    public int getContentViewId() {
        return R.layout.activity_chon_khu_vuc;
    }

    @Override
    public void initializeComponents() {
        Log.d("RUNNING","Chon Khu Vuc Activity");
        btnContinue = findViewById(R.id.btnContinue);
        tvKhuVuc = findViewById(R.id.tvKhuVuc);
        btnContinue.setOnClickListener(this);
        tvKhuVuc.setOnClickListener(this);
        //test
        NotifyDetail notifyDetail = new NotifyDetail();
        notifyDetail.setMessage("Test notify");
        notifyDetail.setTitle("Day la title");
        Notify notify = new Notify();
        notify.setData(notifyDetail);

//        notify.setTo("e0q1v69iAaE:APA91bH3gfehqIm05vvdQTih44wTyArKm00e4HeS2htNjkjnVYk9z7dmxnpeHT5PVsHsDNymUPAwotl6HH5RlaEYhHPS1EEFc_5VeMiCBiD8w5YkiOkCFQEA63uaMieqNVqGEEgn1Plu6LwTAiqg6tjD1v7eULJ_mA");
        notify.setTo("dEpNJiA-438:APA91bGUtf1Hh7tJVSAK3z-EHjq2U6wPDUnGWyBZ4m_Rma7x-t7Kgg0aMLZSBxMqkUGAV_p0HDWVMCPypsPLKUIjBl-ScJr-901RzQJlsdm1XB48WY5XnGHg27iAQMQtfo1pSTRFaLShlR91J23Qi1-PGMGIVm_xFw");
        FCMApiUtils.getFCMCService().sendNotify(notify).enqueue(new Callback<Notify>() {
            @Override
            public void onResponse(Call<Notify> call, Response<Notify> response) {
                Toast.makeText(ChonKhuVucActivity.this, "Success", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Notify> call, Throwable t) {
                Toast.makeText(ChonKhuVucActivity.this, "Fail", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public ChonKhuVucPresenter createPresenter() {
        return new ChonKhuVucPresenterImp(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnContinue:
                getPresenter().onClickContinue(tvKhuVuc.getText().toString());
                break;
            case R.id.tvKhuVuc:
                Intent intent = new Intent(ChonKhuVucActivity.this, ChonKhuVucActivity2.class);
                startActivityForResult(intent, Key.CODE_REQUEST_KV_2);
                break;
        }

    }

    @Override
    public void onClickContinue(String s) {
        Intent intent = new Intent(ChonKhuVucActivity.this, MainActivity.class);
        intent.putExtra(Key.CODE_KHU_VUC, s);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Key.CODE_REQUEST_KV_2 && resultCode == RESULT_OK) {
            KhuVuc khuVuc = (KhuVuc) data.getSerializableExtra(Key.KHU_VUC);
            tvKhuVuc.setText(khuVuc.getName());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LoginActivity.isAppRunning = false;
    }
}
