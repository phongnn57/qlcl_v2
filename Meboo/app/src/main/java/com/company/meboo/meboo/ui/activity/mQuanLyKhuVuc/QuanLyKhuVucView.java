package com.company.meboo.meboo.ui.activity.mQuanLyKhuVuc;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mQuanLyKhuVucPresenter.QuanLyKhuVucPresenter;

public interface QuanLyKhuVucView extends BaseView<QuanLyKhuVucPresenter>{
    void notifyRegionData();

    Context gContext();
}
