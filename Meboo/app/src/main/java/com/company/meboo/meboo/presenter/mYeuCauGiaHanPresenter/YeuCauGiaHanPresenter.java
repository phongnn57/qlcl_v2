package com.company.meboo.meboo.presenter.mYeuCauGiaHanPresenter;

import com.company.meboo.meboo.base.BasePresenter;

public interface YeuCauGiaHanPresenter extends BasePresenter{
    void getNotifyDetail(int i);

    void sendNewReport(String id, String newDeadLine, String desc);
}
