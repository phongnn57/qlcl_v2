package com.company.meboo.meboo.ui.activity.mChonKhuVucActivity2;

import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.model.model.object.KhuVuc;
import com.company.meboo.meboo.presenter.mChonKhuVucPresenter2.ChonKhuVucPresenter2;
import com.company.meboo.meboo.presenter.mChonKhuVucPresenter2.ChonKhuVucPresenterImp2;
import com.company.meboo.meboo.ui.activity.mLoginActivity.LoginActivity;
import com.company.meboo.meboo.ui.adapter.ChonKhuVucAdapter;
import com.company.meboo.meboo.util.Helper;
import com.company.meboo.meboo.util.Key;

public class ChonKhuVucActivity2 extends BaseActivity<ChonKhuVucPresenter2> implements ChonKhuVucView2, SearchView.OnQueryTextListener {
    RecyclerView recyclerView;
    ChonKhuVucAdapter chonKhuVucAdapter;
    @Override
    public int getContentViewId() {
        return R.layout.activity_chon_khu_vuc2;
    }

    @Override
    public void initializeComponents() {
        setSupportActionBar((Toolbar) findViewById(R.id.myToolbar));
        setupUI(findViewById(R.id.parent));
        recyclerView = findViewById(R.id.recyclerView);
        //setup recyclerview
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
        //setup adapter
        chonKhuVucAdapter = new ChonKhuVucAdapter(this,getPresenter().getListKhuVuc());
        recyclerView.setAdapter(chonKhuVucAdapter);
        chonKhuVucAdapter.setOnItemClickListener(new ChonKhuVucAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                getPresenter().chonKhuVuc(position);
            }
        });
    }

    @Override
    public ChonKhuVucPresenter2 createPresenter() {
        return new ChonKhuVucPresenterImp2(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_chon_kv,menu);
        MenuItem item = menu.findItem(R.id.searchView);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint("Tìm kiếm...");
        searchView.setSubmitButtonEnabled(true);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        chonKhuVucAdapter.getFilter().filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        chonKhuVucAdapter.getFilter().filter(newText);
        return false;
    }

    public void setupUI(View view) {
        if (!(view instanceof SearchView)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    Helper.hideSoftKeyboard(ChonKhuVucActivity2.this);
                    return false;
                }
            });
        }
    }

    @Override
    public void chonKhuVuc(KhuVuc khuVuc) {
        Toast.makeText(this, khuVuc.getName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        intent.putExtra(Key.KHU_VUC,khuVuc);
        setResult(RESULT_OK, intent);
        finish();
    }
}
