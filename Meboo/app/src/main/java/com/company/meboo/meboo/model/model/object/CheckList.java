package com.company.meboo.meboo.model.model.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CheckList implements Serializable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("desc")
        @Expose
        private String desc;
        @SerializedName("is_oke")
        @Expose
        private Integer isOke;
        @SerializedName("note")
        @Expose
        private Object note;
        @SerializedName("quarter")
        @Expose
        private String quarter;
        @SerializedName("work_type")
        @Expose
        private String workType;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("last_check")
        @Expose
        private String lastCheck;
        @SerializedName("last_check_status")
        @Expose
        private Integer lastCheckStatus;

    public CheckList() {
    }

    public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public Integer getIsOke() {
            return isOke;
        }

        public void setIsOke(Integer isOke) {
            this.isOke = isOke;
        }

        public Object getNote() {
            return note;
        }

        public void setNote(Object note) {
            this.note = note;
        }

        public String getQuarter() {
            return quarter;
        }

        public void setQuarter(String quarter) {
            this.quarter = quarter;
        }

        public String getWorkType() {
            return workType;
        }

        public void setWorkType(String workType) {
            this.workType = workType;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getLastCheck() {
            return lastCheck;
        }

        public void setLastCheck(String lastCheck) {
            this.lastCheck = lastCheck;
        }

        public Integer getLastCheckStatus() {
            return lastCheckStatus;
        }

        public void setLastCheckStatus(Integer lastCheckStatus) {
            this.lastCheckStatus = lastCheckStatus;
        }

    }
