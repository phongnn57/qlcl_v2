package com.company.meboo.meboo.presenter.mThongBaoChiTietPresenter;

import android.util.Log;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.model.object.Checklist2;
import com.company.meboo.meboo.model.model.object.DataReportDetail;
import com.company.meboo.meboo.model.model.object.ReportDetail;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.ui.activity.mThongBaoChiTiet.ThongBaoChiTietView;
import com.company.meboo.meboo.util.PrefUtil;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ThongBaoChiTietPresenterImp extends BasePresenterImpl<ThongBaoChiTietView> implements ThongBaoChiTietPresenter{
    DataReportDetail dataReportDetail = new DataReportDetail();
    public ThongBaoChiTietPresenterImp(ThongBaoChiTietView view) {
        super(view);
    }

    @Override
    public void getNotifyDetail(int i) {
        String token = "bearer "  + PrefUtil.getToken(getView().gContext());
        ApiUtils.getQlclService().getReportDetail(token,i).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ReportDetail>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(ReportDetail reportDetail) {
                        if(reportDetail.getStatus()==1){
                            Log.d("mfirebase","lay du lieu thanh cong");
                            dataReportDetail = reportDetail.getData();
                            getView().setData(dataReportDetail);
                        }
                    }
                });
    }

    public DataReportDetail getDataReportDetail() {
        return dataReportDetail;
    }

    @Override
    public Checklist2 getChecklist() {
        return dataReportDetail.getChecklist();
    }

}
