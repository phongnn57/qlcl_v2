package com.company.meboo.meboo.firebase;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notify {

    @SerializedName("to")
    @Expose
    private String to;
    @SerializedName("data")
    @Expose
    private NotifyDetail data;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public NotifyDetail getData() {
        return data;
    }

    public void setData(NotifyDetail data) {
        this.data = data;
    }

}