package com.company.meboo.meboo.presenter.mCheckListPresenter;

import android.util.Log;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.CheckListModel;
import com.company.meboo.meboo.model.model.object.CheckList;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.network.Response;
import com.company.meboo.meboo.ui.fragment.CheckListFragment.CheckListView;
import com.company.meboo.meboo.util.PrefUtil;

import java.util.ArrayList;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class CheckListPresenterImp extends BasePresenterImpl<CheckListView> implements CheckListPresenter {
    ArrayList<CheckList> listCheckList = new ArrayList<>();
    int last_id = 0;
    int old_last_id = 0;

    public CheckListPresenterImp(CheckListView view) {
        super(view);
        Log.d("ABC","last id" + last_id);
        Log.d("ABC","old id" + old_last_id);
    }

    @Override
    public ArrayList<CheckList> getCheckList() {
        return listCheckList;
    }

    @Override
    public void loadNextData() {
        if(last_id == old_last_id){
            return;
        }
        else{
            initCheckList();
        }
    }

    public void initCheckList() {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());
        Log.d("token", "TOKEN: " + token);
//            String token = "bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6Ly9xbGNsLmZpdG1lLnZuL2FwaS92MS9sb2dpbiIsImlhdCI6MTUzMTkwMTgxMSwiZXhwIjoxNTMxOTA1NDExLCJuYmYiOjE1MzE5MDE4MTEsImp0aSI6ImhSd0FDOVBsU2xYT1ExRFMifQ.pH4JoaYGWnxLiEQH7VQwrZGr4GY6wulL-04D4NXdUvE";
            ApiUtils.getQlclService().getCheckList(token,last_id,10).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Subscriber<Response<ArrayList<CheckList>>>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.d("ABC","" + e.getMessage() + e.toString());
                        }

                        @Override
                        public void onNext(Response<ArrayList<CheckList>> listResponse) {
                            ArrayList<CheckList> listTemp = listResponse.getData();
                            Log.d("ABC","size list temp" + listTemp.size());
                            Log.d("ABC","trong onsuccess");

                            listCheckList.addAll(listTemp);
//                            listCheckList.addAll(listResponse.getData());
                            if (listResponse.getData().size() > 0){
                                old_last_id = last_id;
                                Log.d("ABC",listCheckList.size()+"size listchecklist");
                                last_id = listCheckList.get(listCheckList.size() - 1).getId();
                            }
                            Log.d("ABC",last_id+" old id " +old_last_id);
                            getView().notifyData();
                        }
                    });
        }


}
