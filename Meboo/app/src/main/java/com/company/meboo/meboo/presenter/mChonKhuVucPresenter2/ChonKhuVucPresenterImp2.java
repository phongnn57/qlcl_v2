package com.company.meboo.meboo.presenter.mChonKhuVucPresenter2;

import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.model.KhuVucModel;
import com.company.meboo.meboo.model.model.object.KhuVuc;
import com.company.meboo.meboo.ui.activity.mChonKhuVucActivity2.ChonKhuVucView2;

import java.util.ArrayList;

public class ChonKhuVucPresenterImp2 extends BasePresenterImpl<ChonKhuVucView2> implements ChonKhuVucPresenter2 {
    ArrayList<KhuVuc> listKhuVuc = new ArrayList<>();
    KhuVucModel khuVucModel = new KhuVucModel();

    public ChonKhuVucPresenterImp2(ChonKhuVucView2 view) {
        super(view);
        listKhuVuc = khuVucModel.getListKhuVuc();
    }

    @Override
    public ArrayList<KhuVuc> getListKhuVuc() {
        return listKhuVuc;
    }

    @Override
    public void chonKhuVuc(int position) {
        getView().chonKhuVuc(listKhuVuc.get(position));
    }
}
