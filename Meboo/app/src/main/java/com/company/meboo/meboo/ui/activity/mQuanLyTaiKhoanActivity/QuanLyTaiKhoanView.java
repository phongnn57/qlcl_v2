package com.company.meboo.meboo.ui.activity.mQuanLyTaiKhoanActivity;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mQuanLyTaiKhoanPresenter.QuanLyTaiKhoanPresenter;

public interface QuanLyTaiKhoanView extends BaseView<QuanLyTaiKhoanPresenter>{
    void notifyData();

    Context gContext();
}
