package com.company.meboo.meboo.presenter.mQuanLyTaiKhoanPresenter;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.Account;

import java.util.ArrayList;

public interface QuanLyTaiKhoanPresenter extends BasePresenter{
    ArrayList<Account> getAccountList();
}
