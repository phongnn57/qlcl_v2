package com.company.meboo.meboo.model.model.object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("type")
@Expose
private Integer type;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public Integer getType() {
return type;
}

public void setType(Integer type) {
this.type = type;
}

}