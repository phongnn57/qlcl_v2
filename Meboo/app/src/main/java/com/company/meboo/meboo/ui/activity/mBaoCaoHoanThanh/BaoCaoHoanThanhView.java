package com.company.meboo.meboo.ui.activity.mBaoCaoHoanThanh;

import android.content.Context;

import com.company.meboo.meboo.base.BaseView;
import com.company.meboo.meboo.presenter.mBaoCaoHoanThanhPresenter.BaoCaoHoanThanhPresenter;

public interface BaoCaoHoanThanhView extends BaseView<BaoCaoHoanThanhPresenter>{
    Context gContext();
}
