package com.company.meboo.meboo.presenter.mBaoCaoHoanThanhPresenter;

import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BasePresenterImpl;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.ui.activity.mBaoCaoHoanThanh.BaoCaoHoanThanhView;
import com.company.meboo.meboo.util.PrefUtil;
import com.google.gson.JsonObject;

import java.io.File;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BaoCaoHoanThanhPresenterImp extends BasePresenterImpl<BaoCaoHoanThanhView> implements BaoCaoHoanThanhPresenter{
    public BaoCaoHoanThanhPresenterImp(BaoCaoHoanThanhView view) {
        super(view);
    }

    @Override
    public void sendCompleteReport(String desc, ArrayList<String> filePaths, int id) {
        String token = "bearer " + PrefUtil.getToken(getView().gContext());

        RequestBody rbDesc = RequestBody.create(MediaType.parse("text/plain"),desc);
        RequestBody rbId = RequestBody.create(MediaType.parse("text/plain"),String.valueOf(id));

        ArrayList<MultipartBody.Part> listImage = new ArrayList<>();
        for(String filePath : filePaths){
            File file = new File(filePath);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"),file);
            MultipartBody.Part image = MultipartBody.Part.createFormData("images[]", file.getName(), reqFile);
            listImage.add(image);
        }

        ApiUtils.getQlclService().createCompleteReport(token,listImage,rbId,rbDesc).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(JsonObject jsonObject) {
                        if (jsonObject.get("status").getAsInt() == 1){
                            Toast.makeText(getView().gContext(), "Thêm thành công", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(getView().gContext(), jsonObject.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
