package com.company.meboo.meboo.presenter.mThongBaoPresenter;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.ThongBaoDetail;

import java.util.ArrayList;

public interface ThongBaoPresenter extends BasePresenter{
    ArrayList<ThongBaoDetail> getListNotification();
    void callApiListNotification();
    void loadMore();
    void readAll();

    void read(int id);
}
