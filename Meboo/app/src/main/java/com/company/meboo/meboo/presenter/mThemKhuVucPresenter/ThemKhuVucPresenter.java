package com.company.meboo.meboo.presenter.mThemKhuVucPresenter;

import com.company.meboo.meboo.base.BasePresenter;
import com.company.meboo.meboo.model.model.object.SubRegion;

import java.util.ArrayList;

public interface ThemKhuVucPresenter extends BasePresenter {
    ArrayList<SubRegion> getListRegion();
    void callApiListRegion();

    void createRegion(int parentID, String name, String desc, String filePath);
}
