package com.company.meboo.meboo.presenter.mChiTietYeuCauGiaHanPresenter;

import com.company.meboo.meboo.base.BasePresenter;

import java.util.Date;

public interface ChiTietYeuCauGiaHanPresenter extends BasePresenter {
    void callApiReportExtended(int id);

    void reject(int id, String reasonReject);

    void accept(int id, String date);
}
