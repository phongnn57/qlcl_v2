package com.company.meboo.meboo.network;

import com.company.meboo.meboo.model.model.object.Account;
import com.company.meboo.meboo.model.model.object.AccountLogin;
import com.company.meboo.meboo.model.model.object.CheckList;
import com.company.meboo.meboo.model.model.object.DataReport;
import com.company.meboo.meboo.model.model.object.QuarterReport;
import com.company.meboo.meboo.model.model.object.ReportDetail;
import com.company.meboo.meboo.model.model.object.SubQuarter;
import com.company.meboo.meboo.model.model.object.SubRegion;
import com.company.meboo.meboo.model.model.object.ThongBaoDetail;
import com.company.meboo.meboo.model.model.object.ThongBaoResponse;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

public interface QlclService {
    @GET("/api/v1/checklist/list")
    Observable<Response<ArrayList<CheckList>>> getCheckList(@Header("Authorization") String token,@Query("last_id") int last_id ,@Query("limit") int limit);

    @GET("/api/v1/account/list")
    Observable<Response<ArrayList<Account>>> getAccountList(@Header("Authorization") String token);

    @GET("/api/v1/region/list")
    Observable<Response<DataResponse<SubRegion>>> getRegionList(@Header("Authorization") String token);

    @GET("/api/v1/quarter/list")
    Observable<Response<DataResponse<SubQuarter>>> getQuarterList(@Header("Authorization") String token);

    @POST("api/v1/account/store")
    @FormUrlEncoded
    Observable<JsonObject> sendAccount(@Header("Authorization") String token,
                                       @Field("username") String username,
                                       @Field("password") String password,
                                       @Field("type") int type,
                                       @Field("quarter_id") int quarterID,
                                       @Field("region") ArrayList<SubRegion> regions,
                                       @Field("name") String name,
                                       @Field("phone") String phone,
                                       @Field("email") String email,
                                       @Field("birthday") String birthday
                                       );

    @Multipart
    @POST("api/v1/region/create")
    Observable<Response> createRegion(@Header("Authorization") String token,
                                    @Part("parent_id") RequestBody parentId,
                                    @Part("name") RequestBody name,
                                    @Part("desc") RequestBody desc,
                                    @Part MultipartBody.Part image
    );

    @Multipart
    @POST("api/v1/checklist/create-report")
    Observable<JsonObject> createReport(@Header("Authorization") String token, @Part ArrayList<MultipartBody.Part> part,
                                        @Part("desc") RequestBody desc,
                                        @Part("recommend") RequestBody recommend,
                                        @Part("deadline") RequestBody deadline,
                                        @Part("checklist_id") RequestBody id,
                                        @Part("quarters") ArrayList<String> quarters);

    @POST("/api/v1/login")
    Observable<AccountLogin> login(@Query("username") String name, @Query("password") String password, @Query("android_token") String token);

    @GET("/api/v1/checklist/report-detail")
    Observable<ReportDetail> getReportDetail(@Header("Authorization") String token,
                                             @Query("item_id") int id);

    @GET("/api/v1/notification/list")
    Observable<ThongBaoResponse> getListNotification(@Header("Authorization") String token, @Query("limit") int limit,
                                                     @Query("last_id") int lastId);

    @POST("api/v1/notification/read-all")
    Observable<JsonObject> readAll(@Header("Authorization") String token);

    @Multipart
    @POST("api/v1/checklist/report-extend")
    Observable<JsonObject> createReportExtend(@Header("Authorization") String token,
                                              @Part("report_id") RequestBody id,
                                              @Part("extend_time") RequestBody deadline,
                                              @Part("reason_extend") RequestBody desc);

    @Multipart
    @POST("api/v1/checklist/update-fixed")
    Observable<JsonObject> createCompleteReport(@Header("Authorization") String token, @Part ArrayList<MultipartBody.Part> part,
                                                @Part("item_id") RequestBody id,
                                                @Part("result_fixed") RequestBody desc);

    @GET("/api/v1/checklist/detail-report-extend")
    Observable<Response<DataReport>> getDetailReportExtend(@Header("Authorization") String token,
                                                           @Query("item_id") int id);

    @POST("/api/v1/checklist/accept-extend")
    @FormUrlEncoded
    Observable<JsonObject> acceptExtend(@Header("Authorization") String token, @Field("item_id") int id,
                                        @Field("is_accept") int isAccept, @Field("director_deadline") String date);

    @POST("/api/v1/checklist/accept-extend")
    @FormUrlEncoded
    Observable<JsonObject> rejectExtend(@Header("Authorization") String token, @Field("item_id") int id,
                                        @Field("is_accept") int isAccept, @Field("reason_reject") String reason);

    @POST("/api/v1/notification/read")
    @FormUrlEncoded
    Observable<JsonObject> readNotify(@Header("Authorization") String token, @Field("id") int id);

    @GET("/api/v1/account/report")
    Observable<Response<List<QuarterReport>>> getQuarterReport(@Header("Authorization") String token, @Query("start_time") String startTime, @Query("end_time") String endTime);
}
