package com.company.meboo.meboo.firebase;

import android.database.Observable;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface FirebaseApiService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAMPIXFZM:APA91bFHdMz_U8bfVG9hELlp_p42qd8d8ievHENPHUM6skjbs8WYzANHYtcJQzIPn2R0WJhx92E5aY5l2hTeyYwo-WYW_gyS5mI-YqU0Xgj8pPmCSbbjOdt0bxlCxtbtSGDmVatZ70gPWf6pjP-sAc9_-J6pysPdeg"
            }

    )

    @POST("fcm/send")
    Call<Notify> sendNotify(@Body Notify notify);
}
