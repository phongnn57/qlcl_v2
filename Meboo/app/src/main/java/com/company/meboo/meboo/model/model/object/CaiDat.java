package com.company.meboo.meboo.model.model.object;

public class CaiDat {
    String name;
    int urlImage;
    public CaiDat(int urlImage, String name) {
        this.urlImage = urlImage;
        this.name = name;
    }

    public int getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(int urlImage) {
        this.urlImage = urlImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
