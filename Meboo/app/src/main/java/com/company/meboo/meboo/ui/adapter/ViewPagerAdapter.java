package com.company.meboo.meboo.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class ViewPagerAdapter extends FragmentPagerAdapter {
    ArrayList<Fragment> listFrags;

    public ViewPagerAdapter(FragmentManager fm, ArrayList<Fragment> listFrags) {
        super(fm);
        this.listFrags = listFrags;
    }

    @Override
    public Fragment getItem(int position) {
        return listFrags.get(position);
    }

    @Override
    public int getCount() {
        return listFrags.size();
    }
}
