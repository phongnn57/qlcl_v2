package com.company.meboo.meboo.ui.activity.mThongBaoChiTiet;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.model.model.object.CheckList;
import com.company.meboo.meboo.model.model.object.Checklist2;
import com.company.meboo.meboo.model.model.object.DataReportDetail;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.presenter.mThongBaoChiTietPresenter.ThongBaoChiTietPresenter;
import com.company.meboo.meboo.presenter.mThongBaoChiTietPresenter.ThongBaoChiTietPresenterImp;
import com.company.meboo.meboo.ui.activity.mBaoCaoHoanThanh.BaoCaoHoanThanhActivity;
import com.company.meboo.meboo.ui.activity.mYeuCauGiaHan.YeuCauGiaHanActivity;
import com.company.meboo.meboo.util.Key;
import com.squareup.picasso.Picasso;

import me.relex.circleindicator.CircleIndicator;

public class ThongBaoChiTietActivity extends BaseActivity<ThongBaoChiTietPresenter> implements ThongBaoChiTietView, View.OnClickListener {
    TextView tvNameChecklist,tvQuarterChecklist, tvDescChecklist, tvDesc, tvRecommend, tvDeadLine;
    ImageView imvProve, imvFixed;
    TextView tvFixed;
    DataReportDetail dataReportDetail= new DataReportDetail();
    Button btnYCGH, btnConfirm;
    LinearLayout lnNonFixed, lnFixed;
    int i;

    @Override
    public int getContentViewId() {
        return R.layout.activity_thong_bao_chi_tiet;
    }

    @Override
    public void initializeComponents() {
        setSupportActionBar((Toolbar) findViewById(R.id.myToolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        findViewByIDs();

        i = getIntent().getIntExtra(Key.ID,0);
        if(i!=0){
            getPresenter().getNotifyDetail(i);
        }
    }


    public void findViewByIDs(){
        tvNameChecklist = findViewById(R.id.tvNameChecklist);
        lnFixed = findViewById(R.id.ln_fixed);
        lnNonFixed = findViewById(R.id.ln_non_fixed);
        tvFixed = findViewById(R.id.tvFixed);
        imvFixed = findViewById(R.id.imvFixed);
        tvQuarterChecklist = findViewById(R.id.tvQuarterChecklist);
        tvDescChecklist = findViewById(R.id.tvDescChecklist);
        tvDesc = findViewById(R.id.tvDesc);
        tvRecommend = findViewById(R.id.tvRecommend);
        tvDeadLine = findViewById(R.id.tvDeadLine);
        imvProve = findViewById(R.id.imvProve);
        btnYCGH = findViewById(R.id.btnYCGH);
        btnConfirm = findViewById(R.id.btnConfirm);

        btnYCGH.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public ThongBaoChiTietPresenter createPresenter() {
        return new ThongBaoChiTietPresenterImp(this);
    }

    @Override
    public Context gContext() {
        return this;
    }

    @Override
    public void setData(DataReportDetail dataReportDetail) {
        this.dataReportDetail = dataReportDetail;
        Log.d("mfirebase",dataReportDetail.getChecklist().getQuarterName() + "");
        if(dataReportDetail.getChecklist().getName()!=null){
            tvNameChecklist.setText(dataReportDetail.getChecklist().getName().toString());
        }else tvNameChecklist.setText("NULL");

        tvQuarterChecklist.setText(dataReportDetail.getChecklist().getQuarterName().toString());
        tvDescChecklist.setText(dataReportDetail.getChecklist().getDesc().toString());
        tvDesc.setText(dataReportDetail.getDesc().toString());
        tvDeadLine.setText(dataReportDetail.getDeadline().toString());
        if(dataReportDetail.getRecommend()!=null){

            tvRecommend.setText(dataReportDetail.getRecommend().toString());
        }else tvRecommend.setText("NULL");


        if(!dataReportDetail.getImages().isEmpty()){
            Log.d("mfirebase","Image: " + dataReportDetail.getImages().get(0));
            Picasso.with(this).load(ApiUtils.BASE_URL + dataReportDetail.getImages().get(0)).into(imvProve);
        }else Log.d("mfirebase","Image null");

        if(dataReportDetail.getIsFixed()==1){
            lnNonFixed.setVisibility(View.GONE);
            lnFixed.setVisibility(View.VISIBLE);
            if (dataReportDetail.getImagesFixed() != null)
                Glide.with(this).load(ApiUtils.BASE_URL + dataReportDetail.getImagesFixed()).into(imvFixed);
            if (dataReportDetail.getResultFixed() != null) tvFixed.setText(dataReportDetail.getResultFixed() + "");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnYCGH:
                Intent intent = new Intent(this, YeuCauGiaHanActivity.class);
                intent.putExtra(Key.ID,i);
                startActivity(intent);
                break;
            case R.id.btnConfirm:
                Intent intent2 = new Intent(this, BaoCaoHoanThanhActivity.class);
                intent2.putExtra(Key.NAME_CHECK_LIST,tvNameChecklist.getText().toString());
                intent2.putExtra(Key.QUARTER_CHECK_LIST,tvQuarterChecklist.getText().toString());
                intent2.putExtra(Key.DESC_CHECK_LIST,tvDesc.getText().toString());
                intent2.putExtra(Key.ID,i);
                startActivity(intent2);
                break;
        }
    }
}
