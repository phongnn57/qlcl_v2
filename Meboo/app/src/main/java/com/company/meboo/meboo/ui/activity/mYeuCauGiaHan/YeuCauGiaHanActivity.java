package com.company.meboo.meboo.ui.activity.mYeuCauGiaHan;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.company.meboo.meboo.R;
import com.company.meboo.meboo.base.BaseActivity;
import com.company.meboo.meboo.model.model.object.DataReportDetail;
import com.company.meboo.meboo.network.ApiUtils;
import com.company.meboo.meboo.presenter.mYeuCauGiaHanPresenter.YeuCauGiaHanPresenter;
import com.company.meboo.meboo.presenter.mYeuCauGiaHanPresenter.YeuCauGiaHanPresenterImp;
import com.company.meboo.meboo.ui.activity.mBaoCaoXuLy.BaoCaoXuLyActivity;
import com.company.meboo.meboo.util.Helper;
import com.company.meboo.meboo.util.Key;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class YeuCauGiaHanActivity extends BaseActivity<YeuCauGiaHanPresenter> implements YeuCauGiaHanView, View.OnClickListener {
    DataReportDetail dataReportDetail = new DataReportDetail();
    TextView tvNameChecklist, tvQuarterChecklist, tvDeadLine, tvDescChecklist, tvNewDeadLine;
    EditText edtReasonDeadLine;
    String tvTime = "";
    Button btnConfirm;
    int i;

    @Override
    public int getContentViewId() {
        return R.layout.activity_yeu_cau_gia_han;
    }

    @Override
    public void initializeComponents() {
        tvNameChecklist = findViewById(R.id.tvNameChecklist);
        tvQuarterChecklist = findViewById(R.id.tvQuarterChecklist);
        tvDescChecklist = findViewById(R.id.tvDescChecklist);
        tvDeadLine = findViewById(R.id.tvDeadLine);
        edtReasonDeadLine = findViewById(R.id.tvReasonDeadLine);
        btnConfirm = findViewById(R.id.btnConfirm);
        tvNewDeadLine = findViewById(R.id.tvNewDeadLine);
        i = getIntent().getIntExtra(Key.ID, 0);
        if (i != 0) {
            getPresenter().getNotifyDetail(i);
        }
        tvNewDeadLine.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);
    }

    @Override
    public YeuCauGiaHanPresenter createPresenter() {
        return new YeuCauGiaHanPresenterImp(this);
    }

    @Override
    public Context gContext() {
        return this;
    }

    @Override
    public void setData(DataReportDetail dataReportDetail) {
        this.dataReportDetail = dataReportDetail;
        Log.d("mfirebase", dataReportDetail.getChecklist().getQuarterName() + "");
        if (dataReportDetail.getChecklist().getName() != null) {
            tvNameChecklist.setText(dataReportDetail.getChecklist().getName().toString());
        } else tvNameChecklist.setText("NULL");
        tvDescChecklist.setText(dataReportDetail.getChecklist().getDesc().toString());
        tvQuarterChecklist.setText(dataReportDetail.getChecklist().getQuarterName().toString());
        tvDeadLine.setText(dataReportDetail.getDeadline().toString());


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvNewDeadLine:
                Log.d("YCGH", "onclick tvNewDeadLine");
                Helper.showDataPickerDialog(this, new Helper.OnDateSetListener() {
                    @Override
                    public void onDateSet(Calendar c) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        tvTime = sdf.format(c.getTime());
                        Log.d("TAG", "tv TIme lan 1: " + tvTime);
                        Helper.showTimePickerDialog(YeuCauGiaHanActivity.this, new Helper.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(Calendar c) {
                                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                                tvNewDeadLine.setText(tvNewDeadLine.getText().toString() + " " + sdf.format(c.getTime()));
                                tvTime = tvTime + " " + sdf.format(c.getTime());
                                Log.d("TAG", "tv TIme lan 2: " + tvTime);
                                tvNewDeadLine.setText(tvTime);
                            }
                        });
                    }
                });
                break;
            case R.id.btnConfirm:
                if (!checkEmpty()) {
                    String id = String.valueOf(i);
                    getPresenter().sendNewReport(id, tvNewDeadLine.getText().toString(), edtReasonDeadLine.getText().toString());
                    finish();
                } else {
                    Toast.makeText(this, "Vui lòng nhập đủ thông tin", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public boolean checkEmpty() {
        if (TextUtils.isEmpty(tvNewDeadLine.getText().toString()) && TextUtils.isEmpty(edtReasonDeadLine.getText().toString()) && i != 0)
            return true;
        return false;
    }
}
